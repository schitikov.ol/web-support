# Customer Web Support Application

## Features:
+ responsive design
+ login form authentication
+ role-based authorization
+ OAuth 2
+ create/view tickets with attachments
+ download attachments
+ support for Russian and English application languages
+ changing application language
+ add comments with attachments to tickets
+ online chat with representative (websocket)
+ ajax ticket search with autocomplete
+ pagination
+ server-side form validation
+ track active sessions
+ JS/jQuery animation effects
+ REST and SOAP API

## Tools:
+ **Backend:** JDK 8, Spring 5 (Core, MVC, Data, Security), JPA 2 (Hibernate 5), Bean Validation (Hibernate), Hibernate Search with Apache Lucene, WebSocket, JAXB (OXM), Jackson (JSON2POJO)
+ **Frontend:** JavaScript, Twitter Bootstrap 4, jQuery, JSP, JSTL
+ **Servlet Container:** Tomcat 8.5
+ **Logging:** Log4J
+ **Testing:** JUnit Jupiter, Mockito
+ **Database:** MySQL
+ Maven
+ Git

## Notes:
SQL ddl is located in the database/create.sql

## Screenshots:
### Login page
![](screenshots/login_form.PNG)
### Listing tickets
![](screenshots/tickets_list.PNG)
### Ticket creating
![](screenshots/creating_ticket.PNG)
### Ticket viewing
![](screenshots/showing_ticket.PNG)
### Listing sessions
![](screenshots/sessions.PNG)
### Ticket searching
![](screenshots/searching.PNG)
### Online chat
![](screenshots/chat.PNG)
### Chat requests
![](screenshots/chat_requests.PNG)
### Multi-language support
![](screenshots/language.PNG)