package com.linefight.site.test.chat;

import com.linefight.site.chat.helper.CreateResult;
import com.linefight.site.chat.helper.JoinResult;
import com.linefight.site.chat.model.ChatMessage;
import com.linefight.site.chat.model.ChatSession;
import com.linefight.site.service.ChatService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
abstract class ChatServiceTest {

    ChatService service;
    private CreateResult testCreateResult;

    @BeforeEach
    void setUp() {
        testCreateResult = service.createSession("test");
    }

    @Test
    void createSession() {
        ChatSession firstChatSession = testCreateResult.getChatSession();
        assertNotNull(firstChatSession);
        assertEquals("test", firstChatSession.getCustomerLogin());

        ChatMessage firstCreationMessage = testCreateResult.getCreateMessage();
        assertNotNull(firstCreationMessage);
        assertEquals("test", firstCreationMessage.getUser());
        assertEquals(ChatMessage.Type.STARTED, firstCreationMessage.getType());
        assertEquals("message.chat.started.session", firstCreationMessage.getContentCode());
        assertArrayEquals(new Object[]{"test"}, firstCreationMessage.getContentArguments());
        assertSame(firstCreationMessage, firstChatSession.getCreationMessage());

        CreateResult secondResult = service.createSession("test");
        assertNotSame(testCreateResult, secondResult);
        assertNotEquals(testCreateResult.getChatSession().getSessionId(), secondResult.getChatSession().getSessionId());
    }

    @Test
    void joinSession() {
        JoinResult joinResult = service.joinSession(testCreateResult.getChatSession().getSessionId(), "anotherTest");
        assertNotNull(joinResult);

        ChatSession joinChatSession = joinResult.getChatSession();
        assertNotNull(joinChatSession);
        assertEquals("test", joinChatSession.getCustomerLogin());
        assertEquals("anotherTest", joinChatSession.getRepresentativeLogin());

        ChatMessage joinMessage = joinResult.getJoinMessage();
        assertNotNull(joinMessage);
        assertEquals("anotherTest", joinMessage.getUser());
        assertEquals(ChatMessage.Type.JOINED, joinMessage.getType());
        assertEquals("message.chat.joined.session", joinMessage.getContentCode());
        assertArrayEquals(new Object[]{"anotherTest"}, joinMessage.getContentArguments());

        JoinResult repeatedJoinResult = service.joinSession(testCreateResult.getChatSession().getSessionId(), "anotherTest");
        assertNull(repeatedJoinResult);

        JoinResult nonExistentJoinResult = service.joinSession(testCreateResult.getChatSession().getSessionId() + 100, "anotherTest");
        assertNull(nonExistentJoinResult);
    }

    @Test
    void leaveSessionNormal() {
        ChatMessage message = service.leaveSession(testCreateResult.getChatSession(), "test", ChatService.ReasonForLeaving.NORMAL);
        assertNotNull(message);

        assertEquals("message.chat.left.chat.normal", message.getContentCode());
        assertArrayEquals(new Object[]{"test"}, message.getContentArguments());

        assertEquals(ChatMessage.Type.LEFT, message.getType());

        JoinResult joinToLeavedSessionResult = service.joinSession(testCreateResult.getChatSession().getSessionId(), "test");
        assertNull(joinToLeavedSessionResult, "JoinResult to leaved session should be null");
    }

    @Test
    void leaveSessionLoggedOut() {
        ChatMessage message = service.leaveSession(testCreateResult.getChatSession(), "test", ChatService.ReasonForLeaving.LOGGED_OUT);
        assertNotNull(message);

        assertEquals("message.chat.logged.out", message.getContentCode());
        assertArrayEquals(new Object[]{"test"}, message.getContentArguments());

        assertEquals(ChatMessage.Type.LEFT, message.getType());

        JoinResult joinToLeavedSessionResult = service.joinSession(testCreateResult.getChatSession().getSessionId(), "test");
        assertNull(joinToLeavedSessionResult, "JoinResult to leaved session should be null");
    }

    @Test
    void leaveSessionError() {
        ChatMessage message = service.leaveSession(testCreateResult.getChatSession(), "test", ChatService.ReasonForLeaving.ERROR);
        assertNotNull(message);

        assertEquals("message.chat.left.chat.error", message.getContentCode());
        assertArrayEquals(new Object[]{"test"}, message.getContentArguments());

        assertEquals(ChatMessage.Type.ERROR, message.getType());

        JoinResult joinToLeavedSessionResult = service.joinSession(testCreateResult.getChatSession().getSessionId(), "test");
        assertNull(joinToLeavedSessionResult, "JoinResult to leaved session should be null");
    }

    @Test
    void getPendingSessions() {
        List<ChatSession> expectedPendingSessions = new ArrayList<>();
        expectedPendingSessions.add(testCreateResult.getChatSession());

        for (int i = 0; i < 5; i++) {
            expectedPendingSessions.add(service.createSession("test" + i).getChatSession());
        }

        assertEquals(expectedPendingSessions.size(), service.getPendingSessions().size());
        List<ChatSession> actualPendingSessions = service.getPendingSessions();
        assertTrue(actualPendingSessions.containsAll(expectedPendingSessions));

        ChatSession leavedChatSession = expectedPendingSessions.remove(0);
        service.leaveSession(leavedChatSession, "test", ChatService.ReasonForLeaving.NORMAL);

        assertEquals(expectedPendingSessions.size(), service.getPendingSessions().size());
        actualPendingSessions = service.getPendingSessions();
        assertTrue(actualPendingSessions.containsAll(expectedPendingSessions));

        ChatSession joinedChatSession = expectedPendingSessions.remove(0);
        service.joinSession(joinedChatSession.getSessionId(), "test");

        assertEquals(expectedPendingSessions.size(), service.getPendingSessions().size());
        actualPendingSessions = service.getPendingSessions();
        assertTrue(actualPendingSessions.containsAll(expectedPendingSessions));
    }
}