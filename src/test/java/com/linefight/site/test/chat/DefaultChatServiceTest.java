package com.linefight.site.test.chat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linefight.site.service.impl.DefaultChatService;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class DefaultChatServiceTest extends ChatServiceTest {

    @Mock
    private ObjectMapper objectMapper;

    @InjectMocks
    private DefaultChatService service;

    @BeforeEach
    void setUp() {
        super.service = service;
        super.setUp();
    }

}
