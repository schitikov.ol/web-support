package com.linefight.site.test.user;

import com.linefight.site.entities.UserPrincipal;
import com.linefight.site.repository.UserRepository;
import com.linefight.site.test.GenericTestRepository;

public class UserRepositoryImpl extends GenericTestRepository<UserPrincipal> implements UserRepository {
    public UserRepositoryImpl(Class<? extends UserPrincipal> clazz) {
        super(clazz);
    }

    @Override
    public UserPrincipal getByLogin(String login) {
        return database.values().stream().filter(u -> login.equals(u.getLogin())).findFirst().orElse(null);
    }
}
