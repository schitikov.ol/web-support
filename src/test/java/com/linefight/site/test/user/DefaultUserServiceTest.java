package com.linefight.site.test.user;

import com.linefight.site.entities.UserPrincipal;
import com.linefight.site.service.impl.DefaultUserService;
import org.junit.jupiter.api.BeforeEach;

class DefaultUserServiceTest extends UserServiceTest {

    @BeforeEach
    public void setUp() {
        userRepository = new UserRepositoryImpl(UserPrincipal.class);
        service = new DefaultUserService(userRepository);
        super.setUp();
    }
}
