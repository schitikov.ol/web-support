package com.linefight.site.test.user;

import com.linefight.site.entities.UserPrincipal;
import com.linefight.site.repository.UserRepository;
import com.linefight.site.service.UserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCrypt;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public abstract class UserServiceTest {

    UserService service;

    private final UserPrincipal userPrincipal = new UserPrincipal();
    UserRepository userRepository;

    @BeforeEach
    public void setUp() {
        userPrincipal.setId(1);
        userPrincipal.setLogin("test");
        userPrincipal.setLocale("en_US");
        userPrincipal.setAccountNonExpired(true);
        userPrincipal.setAccountNonLocked(true);
        userPrincipal.setHashedPassword("$2a$10$1Tb9uH33ESHmHAos8gMW6uOfqTtHoZHhLL6.4.axXcw.K6kYgYXne".getBytes(UTF_8));

        userRepository.save(userPrincipal);
    }

    @AfterEach
    public void tearDown() {
        userRepository.deleteAll();
    }

    @Test
    void loadUserByUsername() {
        UserPrincipal principal = service.loadUserByUsername(userPrincipal.getUsername());
        assertNotNull(principal);
        assertEquals(userPrincipal, principal);
    }

    @Test
    void changeUserLocale() {
        service.changeUserLocale(userPrincipal, "ru");
        UserPrincipal loadedPrincipal = service.loadUserByUsername(userPrincipal.getUsername());

        assertEquals("ru", userPrincipal.getLocale());
        assertEquals("ru", loadedPrincipal.getLocale());
    }

    @Test
    void testSaveUser() {
        service.saveUser(userPrincipal, "newPassword");
        UserPrincipal loadedPrincipal = service.loadUserByUsername(userPrincipal.getUsername());

        assertTrue(BCrypt.checkpw("newPassword", userPrincipal.getPassword()));
        assertTrue(BCrypt.checkpw("newPassword", loadedPrincipal.getPassword()));
    }
}