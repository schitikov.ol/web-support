package com.linefight.site.test;

import org.springframework.data.repository.CrudRepository;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class GenericTestRepository<T> implements CrudRepository<T, Long> {

    protected Map<Long, T> database = new HashMap<>();

    private Method setId;
    private Method getId;

    private long idSequence = 0L;

    public GenericTestRepository(Class<? extends T> clazz) {
        try {
            setId = clazz.getMethod("setId", long.class);
            getId = clazz.getMethod("getId");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings({"ConstantConditions", "unchecked"})
    @Override
    public <S extends T> S save(S entity) {
        T savedEntity = null;

        try {
            if ((long) getId.invoke(entity) <= 0L)
                setId.invoke(entity, idSequence++);
            savedEntity = database.put((Long) getId.invoke(entity), entity);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return (S) savedEntity;
    }

    @Override
    public <S extends T> Iterable<S> saveAll(Iterable<S> entities) {
        List<S> saved = new ArrayList<>();
        entities.forEach(e -> saved.add(save(e)));
        return saved;
    }

    @Override
    public Optional<T> findById(Long aLong) {
        return Optional.ofNullable(database.get(aLong));
    }

    @Override
    public boolean existsById(Long aLong) {
        return database.containsKey(aLong);
    }

    @Override
    public Iterable<T> findAll() {
        return database.values();
    }

    @Override
    public Iterable<T> findAllById(Iterable<Long> longs) {
        List<T> found = new ArrayList<>();
        for (long l : longs)
            if (database.containsKey(l))
                found.add(database.get(l));
        return found;
    }

    @Override
    public long count() {
        return database.size();
    }

    @Override
    public void deleteById(Long aLong) {
        database.remove(aLong);
    }

    @Override
    public void delete(T entity) {
        try {
            deleteById((Long) getId.invoke(entity));
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAll(Iterable<? extends T> entities) {
        for (T e : entities)
            delete(e);
    }

    @Override
    public void deleteAll() {
        database.clear();
    }
}
