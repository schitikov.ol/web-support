package com.linefight.site.test.ticket;

import com.linefight.site.entities.TicketComment;
import com.linefight.site.repository.TicketCommentRepository;
import com.linefight.site.test.GenericTestRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.stream.Collectors;

public class TicketCommentRepositoryImpl extends GenericTestRepository<TicketComment> implements TicketCommentRepository {
    public TicketCommentRepositoryImpl(Class<? extends TicketComment> clazz) {
        super(clazz);
    }

    @Override
    public Page<TicketComment> getByTicketId(long ticketId, Pageable p) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterable<TicketComment> getByTicketId(long ticketId) {
        return database.values().stream()
                .filter(ticketComment -> ticketComment.getTicket().getId() == ticketId)
                .collect(Collectors.toList());
    }
}
