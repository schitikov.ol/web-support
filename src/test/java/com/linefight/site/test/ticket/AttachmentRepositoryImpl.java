package com.linefight.site.test.ticket;

import com.linefight.site.entities.Attachment;
import com.linefight.site.repository.AttachmentRepository;
import com.linefight.site.test.GenericTestRepository;

public class AttachmentRepositoryImpl extends GenericTestRepository<Attachment> implements AttachmentRepository {
    public AttachmentRepositoryImpl(Class<? extends Attachment> clazz) {
        super(clazz);
    }
}
