package com.linefight.site.test.ticket;

import com.linefight.site.entities.Attachment;
import com.linefight.site.entities.Ticket;
import com.linefight.site.entities.TicketComment;
import com.linefight.site.service.impl.DefaultTicketService;
import org.junit.jupiter.api.BeforeEach;

public class DefaultTicketServiceTest extends TicketServiceTest {

    @Override
    @BeforeEach
    void setUp() {

        ticketRepository = new TicketRepositoryImpl(Ticket.class);
        ticketCommentRepository = new TicketCommentRepositoryImpl(TicketComment.class);
        attachmentRepository = new AttachmentRepositoryImpl(Attachment.class);

        service = new DefaultTicketService(ticketRepository, attachmentRepository, ticketCommentRepository);

        super.setUp();
    }
}
