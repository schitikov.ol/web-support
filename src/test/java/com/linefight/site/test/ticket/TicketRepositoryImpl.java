package com.linefight.site.test.ticket;

import com.linefight.site.entities.Ticket;
import com.linefight.site.model.SearchResult;
import com.linefight.site.repository.TicketRepository;
import com.linefight.site.test.GenericTestRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class TicketRepositoryImpl extends GenericTestRepository<Ticket> implements TicketRepository {
    public TicketRepositoryImpl(Class<? extends Ticket> clazz) {
        super(clazz);
    }

    @Override
    public Page<SearchResult<Ticket>> search(String query, Pageable page) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterable<Ticket> findAll(Sort sort) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Page<Ticket> findAll(Pageable pageable) {
        throw new UnsupportedOperationException();
    }
}
