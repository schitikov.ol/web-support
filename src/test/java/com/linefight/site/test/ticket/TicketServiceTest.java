package com.linefight.site.test.ticket;

import com.linefight.site.entities.Attachment;
import com.linefight.site.entities.Ticket;
import com.linefight.site.entities.TicketComment;
import com.linefight.site.entities.UserPrincipal;
import com.linefight.site.repository.AttachmentRepository;
import com.linefight.site.repository.TicketCommentRepository;
import com.linefight.site.repository.TicketRepository;
import com.linefight.site.service.TicketService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
abstract class TicketServiceTest {

    TicketService service;
    private final UserPrincipal userPrincipal = new UserPrincipal();
    TicketRepository ticketRepository;
    TicketCommentRepository ticketCommentRepository;
    AttachmentRepository attachmentRepository;
    private List<Ticket> tickets = new ArrayList<>();

    @BeforeEach
    void setUp() {

        userPrincipal.setId(1);
        userPrincipal.setLogin("test");
        userPrincipal.setLocale("en_US");
        userPrincipal.setAccountNonExpired(true);
        userPrincipal.setAccountNonLocked(true);
        userPrincipal.setHashedPassword("$2a$10$1Tb9uH33ESHmHAos8gMW6uOfqTtHoZHhLL6.4.axXcw.K6kYgYXne".getBytes(UTF_8));

        for (int i = 0; i < 10; i++) {
            Ticket ticket = new Ticket();
            ticket.setBody("Test problem " + i);
            ticket.setSubject("Problem #" + i);
            ticket.setCustomer(userPrincipal);
            ticket.setDateCreated(Instant.now());

            for (int j = 0; j < 5; j++) {
                Attachment attachment = new Attachment();
                attachment.setContents(new byte[]{0});
                attachment.setContentType("image/gif");
                attachment.setName("Attachment #" + j);
                ticket.addAttachment(attachment);
                attachmentRepository.save(attachment);
            }

            List<TicketComment> comments = new ArrayList<>();
            for (int j = 0; j < 5; j++) {
                TicketComment comment = new TicketComment();
                comment.setBody("Comment #" + j);
                comment.setCustomer(userPrincipal);
                comment.setTicket(ticket);
                comment.setDateCreated(Instant.now());
                comments.add(comment);
                ticketCommentRepository.save(comment);
            }
            ticket.setComments(comments);

            tickets.add(ticket);
            ticketRepository.save(ticket);
        }
    }

    @AfterEach
    void tearDown() {
        ticketRepository.deleteAll();
        ticketCommentRepository.deleteAll();
        attachmentRepository.deleteAll();
    }

    @Test
    void getAllTickets() {
        List<Ticket> allTickets = new ArrayList<>();

        service.getAllTickets(t -> t.setSubject("test")).forEach(allTickets::add);

        assertTrue(tickets.containsAll(allTickets));

        for (Ticket t : allTickets)
            assertEquals("test", t.getSubject());

        for (Ticket t : tickets)
            assertEquals("test", t.getSubject());
    }

    @Test
    void getTicket() {
        assertEquals(tickets.get(0), service.getTicket(tickets.get(0).getId()));
    }

    @Test
    void createTicket() {
        Ticket ticket = new Ticket();
        ticket.setSubject("test");
        ticket.setBody("test");
        ticket.setCustomer(userPrincipal);
        ticket.setDateCreated(Instant.now());

        service.create(ticket);

        assertEquals(ticket, ticketRepository.findById(ticket.getId()).get());
    }

    @Test
    void updateTicket() {
        Ticket ticket = tickets.get(0);
        ticket.setSubject("test");

        service.update(ticket);

        assertEquals("test", ticketRepository.findById(ticket.getId()).get().getSubject());
    }

    @Test
    void deleteTicket() {
        service.deleteTicket(tickets.get(0).getId());
        assertFalse(ticketRepository.existsById(tickets.get(0).getId()));
    }

    @Test
    void getComments() {
        Ticket ticket = tickets.get(0);
        List<TicketComment> comments = new ArrayList<>();
        service.getComments(ticket.getId(), c -> c.setBody("test")).forEach(comments::add);

        assertTrue(ticket.getComments().containsAll(comments));

        for (TicketComment c : comments)
            assertEquals("test", c.getBody());

        for (TicketComment c : ticket.getComments())
            assertEquals("test", c.getBody());
    }

    @Test
    void createComment() {
        TicketComment comment = new TicketComment();
        comment.setBody("test");
        comment.setCustomer(userPrincipal);

        service.create(comment, tickets.get(0).getId());

        assertEquals(tickets.get(0), comment.getTicket());
        assertTrue(ticketCommentRepository.existsById(comment.getId()));
    }

    @Test
    void updateComment() {
        TicketComment comment = tickets.get(0).getComments().get(0);

        comment.setBody("test");

        service.update(comment);

        assertEquals("test", comment.getBody());
        assertEquals("test", ticketCommentRepository.findById(comment.getId()).get().getBody());
    }

    @Test
    void deleteComment() {
        TicketComment comment = tickets.get(0).getComments().get(0);
        service.deleteComment(comment.getId());

        assertFalse(ticketCommentRepository.existsById(comment.getId()));
    }

    @Test
    void getAttachment() {
        Attachment expectedAttachment = tickets.get(0).getAttachments().get(0);
        Attachment actualAttachment = service.getAttachment(expectedAttachment.getId());

        assertEquals(expectedAttachment, actualAttachment);
    }
}