package com.linefight.exception;

public class ResourceNotFoundException extends InternationalizedException {
    public ResourceNotFoundException(String errorCode, Object... arguments) {
        super(errorCode, arguments);
    }
}
