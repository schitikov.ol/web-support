package com.linefight.site.filter;

import org.apache.logging.log4j.ThreadContext;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

public class PreSecurityLoggingFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String id = UUID.randomUUID().toString();
        ThreadContext.put("id", id);

        try {
            ((HttpServletResponse) response).setHeader("X-Request-Id", id);
            chain.doFilter(request, response);
        } finally {
            ThreadContext.remove("id");
            ThreadContext.remove("login");
        }
    }

    @Override
    public void destroy() {

    }
}
