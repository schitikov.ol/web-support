package com.linefight.site.view;

import org.springframework.web.servlet.View;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.Map;

public class DownloadingView implements View {

    private final String filename;
    private final String contentType;
    private final long contentLength;
    private final byte[] contents;

    public DownloadingView(String filename, String contentType, long contentLength, byte[] contents) {
        this.filename = filename;
        this.contentType = contentType;
        this.contents = contents;
        this.contentLength = contentLength;
    }

    @Override
    public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setHeader("Content-Disposition", "attachment; filename*=UTF-8''" +
                URLEncoder.encode(filename, "UTF-8"));
        response.setContentType(contentType);
        response.setContentLengthLong(contentLength);

        response.getOutputStream().write(contents);
    }

    @Override
    public String getContentType() {
        return contentType;
    }
}
