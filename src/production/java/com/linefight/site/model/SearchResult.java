package com.linefight.site.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "http://linefight.com/xmlns/support", name = "result")
public class SearchResult<T> {
    private T entity;
    private double relevance;

    public SearchResult(T entity, double relevance) {
        this.entity = entity;
        this.relevance = relevance;
    }

    public SearchResult() {
    }

    @XmlElement
    @JsonProperty
    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }

    @XmlElement
    @JsonProperty
    public double getRelevance() {
        return relevance;
    }

    public void setRelevance(double relevance) {
        this.relevance = relevance;
    }
}
