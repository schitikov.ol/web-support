package com.linefight.site.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.linefight.site.entities.Ticket;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(namespace = "http://linefight.com/xmlns/support", name = "results")
public class TicketSearchResultList {
    private List<SearchResult<Ticket>> results;

    @XmlElement(name = "result")
    @JsonProperty
    public List<SearchResult<Ticket>> getResults() {
        return results;
    }

    public void setResults(List<SearchResult<Ticket>> results) {
        this.results = results;
    }
}
