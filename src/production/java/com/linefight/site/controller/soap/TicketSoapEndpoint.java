package com.linefight.site.controller.soap;

import com.linefight.site.entities.Attachment;
import com.linefight.site.entities.Ticket;
import com.linefight.site.entities.UserPrincipal;
import com.linefight.site.model.TicketSearchResultList;
import com.linefight.site.model.TicketWebServiceList;
import com.linefight.site.service.TicketService;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ws.server.endpoint.annotation.*;

import javax.inject.Inject;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

//TODO: add comments

@Endpoint
public class TicketSoapEndpoint {
    public static final String NAMESPACE = "http://linefight.com/xmlns/support";

    private final TicketService ticketService;

    private final ApplicationContext context;

    @Inject
    public TicketSoapEndpoint(TicketService ticketService, ApplicationContext context) {
        this.ticketService = ticketService;
        this.context = context;
    }

    @PayloadRoot(namespace = NAMESPACE, localPart = "ticketsRequest")
    @ResponsePayload
    public TicketWebServiceList read(@RequestPayload TicketsRequest request) {
        TicketWebServiceList list = new TicketWebServiceList();
        Pageable pageable = getPageableFrom(request.getPaging());
        list.setValue(ticketService.getAllTickets(pageable,
                t -> t.getAttachments().forEach(Attachment::getContents)).getContent());
        return list;
    }

    @PayloadRoot(namespace = NAMESPACE, localPart = "ticketsSearchRequest")
    @ResponsePayload
    public TicketSearchResultList search(@RequestPayload TicketsSearchRequest request) {
        TicketSearchResultList list = new TicketSearchResultList();
        Pageable pageable = getPageableFrom(request.getPaging());

        list.setResults(ticketService.search(request.getQuery(), pageable,
                t -> t.getAttachments().forEach(Attachment::getContents)).getContent());
        return list;
    }

    @PayloadRoot(namespace = NAMESPACE, localPart = "ticketRequest")
    @Namespace(uri = NAMESPACE, prefix = "s")
    @ResponsePayload
    public Ticket read(@XPathParam("/s:ticketRequest/id") long id) {
        return ticketService.getTicket(id);
    }

    private static UserPrincipal getUserPrincipalFromSecurityContext() {
        return (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    @PayloadRoot(namespace = NAMESPACE, localPart = "deleteTicket")
    @Namespace(uri = NAMESPACE, prefix = "s")
    public void delete(@XPathParam("/s:deleteTicket/id") long id) {
        ticketService.deleteTicket(id);
    }

    @PayloadRoot(namespace = NAMESPACE, localPart = "createTicket")
    @ResponsePayload
    public Ticket create(@RequestPayload CreateTicket form) {
        Ticket ticket = new Ticket();
        ticket.setCustomer(getUserPrincipalFromSecurityContext());
        ticket.setSubject(form.getSubject());
        ticket.setBody(form.getBody());

        if (form.getAttachments() != null) {
            ticket.setAttachments(form.getAttachments());
        }

        ticketService.create(ticket);

        return ticket;
    }

    @XmlRootElement(namespace = NAMESPACE, name = "createTicket")
    public static class CreateTicket {
        private String subject;
        private String body;
        private List<Attachment> attachments;

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        @XmlElement(name = "attachment")
        public List<Attachment> getAttachments() {
            return attachments;
        }

        public void setAttachments(List<Attachment> attachments) {
            this.attachments = attachments;
        }
    }

    private Pageable getPageableFrom(XmlPageable pageable) {
        return PageRequest.of(pageable.getPage() - 1,
                pageable.getSize(),
                Sort.Direction.fromString(pageable.getSortDirection()),
                pageable.getSortProperties()
        );
    }

    @XmlRootElement(namespace = NAMESPACE, name = "paging")
    public static class XmlPageable {

        private int page = 1;
        private int size = 10;
        private String[] sortProperties = {"id"};
        private String sortDirection = "asc";

        @XmlAttribute
        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        @XmlAttribute
        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        @XmlAttribute
        public String getSortDirection() {
            return sortDirection;
        }

        public void setSortDirection(String sortDirection) {
            this.sortDirection = sortDirection;
        }

        @XmlElement(name = "sortProperty")
        public String[] getSortProperties() {
            return sortProperties;
        }

        public void setSortProperties(String[] sortProperties) {
            this.sortProperties = sortProperties;
        }
    }

    @XmlRootElement(namespace = NAMESPACE, name = "ticketsRequest")
    public static class TicketsRequest {
        private XmlPageable paging = new XmlPageable();

        @XmlElement
        public XmlPageable getPaging() {
            return paging;
        }

        public void setPaging(XmlPageable paging) {
            this.paging = paging;
        }
    }

    @XmlRootElement(namespace = NAMESPACE, name = "ticketsSearchRequest")
    private static class TicketsSearchRequest {
        private String query;

        private XmlPageable paging = new XmlPageable();

        @XmlElement
        public String getQuery() {
            return query;
        }

        public void setQuery(String query) {
            this.query = query;
        }

        @XmlElement
        public XmlPageable getPaging() {
            return paging;
        }

        public void setPaging(XmlPageable paging) {
            this.paging = paging;
        }
    }
}
