package com.linefight.site.controller.web.main;

import com.linefight.config.annotation.WebController;
import com.linefight.site.service.ChatService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@WebController
@RequestMapping("chat")
public class ChatController {

    private final ChatService chatService;

    @Inject
    public ChatController(ChatService chatService) {
        this.chatService = chatService;
    }

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(Map<String, Object> model) {
        model.put("sessions", chatService.getPendingSessions());
        return "chat/list";
    }

    @RequestMapping(value = "new", method = RequestMethod.POST)
    public String newChat(Map<String, Object> model, HttpServletResponse response) {
        setNoCacheHeaders(response);
        model.put("chatSessionId", 0);
        return "chat/chat";
    }

    @RequestMapping(value = "join/{chatSessionId}", method = RequestMethod.POST)
    public String joinChat(Map<String, Object> model, HttpServletResponse response,
                           @PathVariable("chatSessionId") long chatSessionId) {
        setNoCacheHeaders(response);
        model.put("chatSessionId", chatSessionId);
        return "chat/chat";
    }

    private void setNoCacheHeaders(HttpServletResponse response) {
        response.setHeader("Expires", "Thu, 1 Jan 1970 12:00:00 GMT");
        response.setHeader("Cache-Control", "max-age=0, must-revalidate, no-cache");
        response.setHeader("Pragma", "no-cache");
    }
}
