package com.linefight.site.controller.web.main;

import com.linefight.config.annotation.WebController;
import com.linefight.site.entities.Attachment;
import com.linefight.site.entities.Ticket;
import com.linefight.site.entities.TicketComment;
import com.linefight.site.entities.UserPrincipal;
import com.linefight.site.service.TicketService;
import com.linefight.site.view.DownloadingView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebController
@RequestMapping("ticket")
public class TicketController {

    private static final Logger log = LogManager.getLogger();

    private final TicketService ticketService;

    @Inject
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @RequestMapping(value = {"", "list"}, method = RequestMethod.GET)
    public String list(Map<String, Object> model, Pageable page) {
        log.debug("Listing tickets.");
        model.put("tickets", ticketService.getAllTickets(page));
        return "ticket/list";
    }

    @RequestMapping(value = "search")
    public ModelAndView search(Map<String, Object> model, @Valid SearchForm searchForm, Errors errors, Pageable page) {
        if (errors.hasErrors()) {
            return getListRedirectModelAndView();
        } else {
            model.put("results", ticketService.search(searchForm.getQuery(), page));
        }

        return new ModelAndView("ticket/search");
    }

    @RequestMapping(value = "show/{ticketId}", method = RequestMethod.GET)
    public ModelAndView show(Map<String, Object> model,
                             @PathVariable("ticketId") long ticketId,
                             Pageable page) {
        Ticket ticket = ticketService.getTicket(ticketId);
        if (ticket == null) {
            return getListRedirectModelAndView();
        }

        model.put("ticketId", Long.toString(ticketId));
        model.put("ticket", ticket);

        if (!model.containsKey("commentForm"))
            model.put("commentForm", new CommentForm());

        model.put("comments", ticketService.getComments(ticketId, page));
        return new ModelAndView("ticket/show");
    }

    @RequestMapping(value = "comment/{ticketId}", method = RequestMethod.POST)
    public ModelAndView comment(@AuthenticationPrincipal UserPrincipal principal, @Valid CommentForm commentForm,
                                Errors errors, Map<String, Object> model, Pageable page,
                                @PathVariable("ticketId") long ticketId) throws IOException {
        Ticket ticket = ticketService.getTicket(ticketId);
        if (ticket == null)
            return getListRedirectModelAndView();
        if (errors.hasErrors())
            return show(model, ticketId, page);

        TicketComment comment = new TicketComment();
        comment.setCustomer(principal);
        comment.setBody(commentForm.getBody());

        for (MultipartFile filePart : commentForm.getAttachments()) {
            log.debug("Processing attachment for new comment.");
            Attachment attachment = new Attachment();
            attachment.setName(filePart.getOriginalFilename());
            attachment.setContentType(filePart.getContentType());
            attachment.setContents(filePart.getBytes());
            if ((attachment.getName() != null && attachment.getName().length() > 0) ||
                    (attachment.getContents() != null && attachment.getContents().length > 0))
                comment.addAttachment(attachment);
        }

        try {
            ticketService.create(comment, ticketId);
        } catch (ConstraintViolationException e) {
            model.put("validationErrors", e.getConstraintViolations());
            return show(model, ticketId, page);
        }

        return new ModelAndView(new RedirectView("/ticket/show/" + ticketId, true, false));
    }

    @RequestMapping(
            value = "attachment/{attachmentId}",
            method = RequestMethod.GET
    )
    public View download(@PathVariable("attachmentId") long attachmentId) {
        Attachment attachment = ticketService.getAttachment(attachmentId);

        if (attachment == null) {
            log.info("Requested attachment with id {} not found.", attachmentId);
            return getListRedirectView();
        }

        return new DownloadingView(attachment.getName(),
                attachment.getContentType(),
                attachment.getContentLength(),
                attachment.getContents());
    }

    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String create(Map<String, Object> model) {
        model.put("ticketForm", new TicketForm());
        return "ticket/add";
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    public ModelAndView create(@AuthenticationPrincipal UserPrincipal principal, @Valid TicketForm ticketForm,
                               Errors errors, Map<String, Object> model) throws IOException {
        if (errors.hasErrors()) {
            return new ModelAndView("ticket/add");
        }

        Ticket ticket = new Ticket();
        ticket.setBody(ticketForm.getBody());
        ticket.setCustomer(principal);
        ticket.setSubject(ticketForm.getSubject());

        for (MultipartFile filePart : ticketForm.getAttachments()) {
            log.debug("Processing attachment for new ticket.");
            Attachment attachment = new Attachment();
            attachment.setContentType(filePart.getContentType());
            attachment.setName(filePart.getOriginalFilename());
            attachment.setContents(filePart.getBytes());

            if ((attachment.getName() != null && attachment.getName().length() > 0) ||
                    (attachment.getContents() != null && attachment.getContents().length > 0)) {
                ticket.addAttachment(attachment);
            }
        }

        try {
            ticketService.create(ticket);
        } catch (ConstraintViolationException e) {
            model.put("validationErrors", e.getConstraintViolations());
            return new ModelAndView("ticket/add");
        }

        return new ModelAndView(new RedirectView("/ticket/show/" + ticket.getId(),
                true, false));
    }

    private ModelAndView getListRedirectModelAndView() {
        return new ModelAndView(getListRedirectView());
    }

    private View getListRedirectView() {
        return new RedirectView("/ticket/list", true, false);
    }

    public static class TicketForm {

        @NotBlank(message = "{validate.ticket.subject}")
        private String subject;

        @NotBlank(message = "{validate.ticket.body}")
        private String body;

        @NotEmpty(message = "{validate.ticket.attachments}")
        private List<MultipartFile> attachments;

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public List<MultipartFile> getAttachments() {
            return attachments;
        }

        public void setAttachments(List<MultipartFile> attachments) {
            this.attachments = attachments;
        }
    }

    public static class CommentForm {

        @NotBlank(message = "{validate.ticket.comment.body}")
        private String body;

        @NotEmpty(message = "{validate.ticket.comment.attachments}")
        private List<MultipartFile> attachments;

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public List<MultipartFile> getAttachments() {
            return attachments;
        }

        public void setAttachments(List<MultipartFile> attachments) {
            this.attachments = attachments;
        }
    }

    public static class SearchForm {

        @NotBlank(message = "{validate.ticket.search.query}")
        private String query;

        private boolean useBooleanMode;

        public String getQuery() {
            return query;
        }

        public void setQuery(String query) {
            this.query = query;
        }

        public boolean isUseBooleanMode() {
            return useBooleanMode;
        }

        public void setUseBooleanMode(boolean useBooleanMode) {
            this.useBooleanMode = useBooleanMode;
        }
    }
}
