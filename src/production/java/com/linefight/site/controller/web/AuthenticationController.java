package com.linefight.site.controller.web;

import com.linefight.config.annotation.WebController;
import com.linefight.site.entities.UserPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.constraints.NotBlank;
import java.util.Map;

@WebController
public class AuthenticationController {

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public ModelAndView login(Map<String, Object> model) {

        if (SecurityContextHolder.getContext().getAuthentication() instanceof UserPrincipal)
            return new ModelAndView(new RedirectView("/ticket/list", true, false));

        model.put("loginForm", new LoginForm());

        return new ModelAndView("login");
    }

    public static class LoginForm {
        @NotBlank(message = "{validate.authenticate.login}")
        private String login;
        @NotBlank(message = "{validate.authenticate.password}")
        private String password;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
}
