package com.linefight.site.controller.web.main;

import com.linefight.config.annotation.WebControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Map;

@WebControllerAdvice
public class MainControllerAdvice {
    @ModelAttribute
    public void addSearchForm(Map<String, Object> model) {
        if (!model.containsKey("searchForm"))
            model.put("searchForm", new TicketController.SearchForm());
    }
}
