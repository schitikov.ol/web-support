package com.linefight.site.controller.web.main;

import com.linefight.config.annotation.WebController;
import org.springframework.web.bind.annotation.RequestMapping;

@WebController
@RequestMapping("oauth")
public class OAuthController {

    @RequestMapping("confirm_access")
    public String authorize() {
        return "oauth/authorize";
    }
}
