package com.linefight.site.controller.rest;

import com.linefight.exception.ResourceNotFoundException;
import com.linefight.site.entities.Attachment;
import com.linefight.site.entities.Ticket;
import com.linefight.site.entities.UserPrincipal;
import com.linefight.site.model.TicketSearchResultList;
import com.linefight.site.model.TicketWebServiceList;
import com.linefight.site.service.TicketService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.inject.Inject;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

//TODO: add discoverability for pagination
//TODO: add comments

@RestController
public class TicketRestController {
    private final TicketService ticketService;

    @Inject
    public TicketRestController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @RequestMapping(value = "ticket", method = RequestMethod.OPTIONS)
    public ResponseEntity<Void> discover() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Allow", "OPTIONS,HEAD,GET,POST");
        return new ResponseEntity<>(null, headers, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "ticket/{id}", method = RequestMethod.OPTIONS)
    public ResponseEntity<Void> discover(@PathVariable("id") long id) {
        if (ticketService.getTicket(id) == null) {
            throw new ResourceNotFoundException("error.rest.ticket.notFound", id);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Allow", "OPTIONS,HEAD,GET,DELETE");
        return new ResponseEntity<>(null, headers, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "ticket", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public TicketWebServiceList read(Pageable page) {
        TicketWebServiceList list = new TicketWebServiceList();
        list.setValue(ticketService.getAllTickets(page,
                t -> t.getAttachments().forEach(Attachment::getContents)).getContent());
        return list;
    }

    @RequestMapping(value = "ticket/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Ticket read(@PathVariable("id") long id) {
        Ticket ticket = ticketService.getTicket(id);
        if (ticket == null) {
            throw new ResourceNotFoundException("error.rest.ticket.notFound", id);
        }
        return ticket;
    }

    @RequestMapping(value = "ticket/search", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public TicketSearchResultList search(@RequestParam String query, Pageable page) {
        TicketSearchResultList list = new TicketSearchResultList();
        list.setResults(ticketService.search(query, page,
                t -> t.getAttachments().forEach(Attachment::getContents)).getContent());
        return list;
    }

    @RequestMapping(value = "ticket", method = RequestMethod.POST)
    public ResponseEntity<Ticket> create(@AuthenticationPrincipal UserPrincipal customer, @RequestBody TicketForm form) {
        Ticket ticket = new Ticket();
        ticket.setCustomer(customer);
        ticket.setSubject(form.getSubject());
        ticket.setBody(form.getBody());
        ticket.setAttachments(form.getAttachments());

        ticketService.create(ticket);

        String uri = ServletUriComponentsBuilder.fromCurrentServletMapping()
                .path("/ticket/{id}").buildAndExpand(ticket.getId()).toString();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Location", uri);

        return new ResponseEntity<>(ticket, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "ticket/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") long id) {
        if (ticketService.getTicket(id) == null)
            throw new ResourceNotFoundException("error.rest.ticket.notFound", id);
        ticketService.deleteTicket(id);
    }

    @XmlRootElement(name = "ticket")
    public static class TicketForm {
        private String subject;
        private String body;
        private List<Attachment> attachments;

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        @XmlElement(name = "attachment")
        public List<Attachment> getAttachments() {
            return attachments;
        }

        public void setAttachments(List<Attachment> attachments) {
            this.attachments = attachments;
        }
    }

    public static class LinkWithHref {
        private String href;

        public LinkWithHref(final String href) {
            this.href = href;
        }

        @XmlAttribute
        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class LinkWithHrefAndRel extends LinkWithHref {
        private String rel;

        public LinkWithHrefAndRel(final String href, final String rel) {
            super(href);
            this.rel = rel;
        }

        @XmlAttribute
        public String getRel() {
            return rel;
        }

        public void setRel(String rel) {
            this.rel = rel;
        }
    }

    @XmlRootElement
    public static class Resorce {
        //TODO:
    }
}
