package com.linefight.site.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.stereotype.Service;

import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

@Service
public class SessionDestroyedListener implements ApplicationListener<SessionDestroyedEvent> {

    private final Set<Consumer<SessionDestroyedEvent>> callbacks = new HashSet<>();

    /*
        Create this data structure to hold callbacks that have been added while callbacks set locked in removeSession.
        If we have a lot of callbacks or callbacks take much time to complete then sessions that have been removed after calling
        removeSession method must wait for releasing lock for a long time. This data structure comes to the rescue.
    */
    private final Set<Consumer<SessionDestroyedEvent>> callbacksAddedWhileLocked = new HashSet<>();

    @Override
    public void onApplicationEvent(SessionDestroyedEvent event) {
        synchronized (callbacks) {
            callbacksAddedWhileLocked.clear();
            callbacks.forEach(c -> c.accept(event));
            try {
                callbacksAddedWhileLocked.forEach(c -> c.accept(event));
            } catch (ConcurrentModificationException ignore) {
            }
        }
    }

    public void registerOnRemoveCallback(Consumer<SessionDestroyedEvent> callback) {
        callbacksAddedWhileLocked.add(callback);
        synchronized (callbacks) {
            callbacks.add(callback);
        }
    }

    public void deregisterOnRemoveCallback(Consumer<SessionDestroyedEvent> callback) {
        synchronized (callbacks) {
            callbacks.remove(callback);
        }
    }
}
