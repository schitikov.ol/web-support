package com.linefight.site.repository;

import com.linefight.site.entities.TicketComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface TicketCommentRepository extends CrudRepository<TicketComment, Long> {
    Page<TicketComment> getByTicketId(long ticketId, Pageable p);

    Iterable<TicketComment> getByTicketId(long ticketId);
}
