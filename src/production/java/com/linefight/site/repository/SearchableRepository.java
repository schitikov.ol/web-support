package com.linefight.site.repository;

import com.linefight.site.model.SearchResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SearchableRepository<T> {
    Page<SearchResult<T>> search(String query, Pageable page);
}
