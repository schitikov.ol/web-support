package com.linefight.site.repository.impl;

import com.linefight.site.entities.Ticket;
import com.linefight.site.model.SearchResult;
import com.linefight.site.repository.SearchableRepository;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.FatalBeanException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.orm.jpa.EntityManagerProxy;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

public class TicketRepositoryImpl implements SearchableRepository<Ticket> {

    @PersistenceContext
    private EntityManager entityManager;

    private EntityManagerProxy entityManagerProxy;

    @Override
    public Page<SearchResult<Ticket>> search(String query,
                                             Pageable pageable) {
        FullTextEntityManager manager = this.getFullTextEntityManager();

        QueryBuilder builder = manager.getSearchFactory().buildQueryBuilder()
                .forEntity(Ticket.class).get();

        Query subjectQuery = builder.keyword()
                .onFields("subject")
                .matching(query)
                .createQuery();

        Query bodyQuery = builder.keyword()
                .onFields("body", "comments.body")
                .matching(query)
                .createQuery();

        Query customerQuery = builder.keyword()
                .onField("customer.login")
                .matching(query)
                .createQuery();

        Query combined = builder.bool()
                .should(subjectQuery)
                .should(bodyQuery)
                .should(customerQuery)
                .createQuery();

        FullTextQuery q = manager.createFullTextQuery(combined, Ticket.class);
        q.setProjection(FullTextQuery.THIS, FullTextQuery.SCORE);
        q.setSort(Sort.RELEVANCE);


        long total = q.getResultSize();

        q.setFirstResult((int) pageable.getOffset())
                .setMaxResults(pageable.getPageSize());

        @SuppressWarnings("unchecked") List<Object[]> results = q.getResultList();
        List<SearchResult<Ticket>> list = new ArrayList<>();
        results.forEach(o -> list.add(
                new SearchResult<>((Ticket) o[0], (Float) o[1])
        ));

        return new PageImpl<>(list, pageable, total);
    }

    private FullTextEntityManager getFullTextEntityManager() {
        return Search.getFullTextEntityManager(
                this.entityManagerProxy.getTargetEntityManager()
        );
    }

    @PostConstruct
    public void initialize() {
        if (!(this.entityManager instanceof EntityManagerProxy))
            throw new FatalBeanException("Entity manager " + this.entityManager +
                    " was not a proxy");

        this.entityManagerProxy = (EntityManagerProxy) this.entityManager;
    }
}
