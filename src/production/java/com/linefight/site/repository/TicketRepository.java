package com.linefight.site.repository;

import com.linefight.site.entities.Ticket;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TicketRepository extends CrudRepository<Ticket, Long>,
        PagingAndSortingRepository<Ticket, Long>, SearchableRepository<Ticket> {

}
