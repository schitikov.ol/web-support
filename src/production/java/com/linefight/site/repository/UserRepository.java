package com.linefight.site.repository;

import com.linefight.site.entities.UserPrincipal;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserPrincipal, Long> {
    UserPrincipal getByLogin(String login);
}

