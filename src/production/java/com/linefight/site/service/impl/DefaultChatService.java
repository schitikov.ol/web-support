package com.linefight.site.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linefight.site.chat.helper.CreateResult;
import com.linefight.site.chat.helper.JoinResult;
import com.linefight.site.chat.model.ChatMessage;
import com.linefight.site.chat.model.ChatSession;
import com.linefight.site.service.ChatService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class DefaultChatService implements ChatService {

    private static final Logger log = LogManager.getLogger();
    private final Map<Long, ChatSession> sessions = new ConcurrentHashMap<>();
    private final Map<Long, ChatSession> pendingSessions = new ConcurrentHashMap<>();
    private final Map<Long, List<ChatMessage>> chatLogs = new ConcurrentHashMap<>();
    private long SESSION_ID_SEQUENCE = 1L;

    private final ObjectMapper objectMapper;

    @Inject
    public DefaultChatService(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public CreateResult createSession(String userLogin) {
        ChatMessage message = new ChatMessage();
        message.setUser(userLogin);
        message.setTimestamp(Instant.now());
        message.setType(ChatMessage.Type.STARTED);
        message.setContentCode("message.chat.started.session");
        message.setContentArguments(userLogin);

        ChatSession session = new ChatSession();
        session.setSessionId(getNextSessionId());
        session.setCustomerLogin(userLogin);
        session.setCreationMessage(message);

        sessions.put(session.getSessionId(), session);
        pendingSessions.put(session.getSessionId(), session);
        chatLogs.put(session.getSessionId(), new ArrayList<>());
        logMessage(session, message);

        return new CreateResult(session, message);
    }

    @Override
    public JoinResult joinSession(long id, String userLogin) {
        ChatSession session = pendingSessions.remove(id);
        if (session == null) {
            log.warn("Attempt to join non-existent session {}.", id);
            return null;
        }
        session.setRepresentativeLogin(userLogin);

        ChatMessage message = new ChatMessage();
        message.setUser(userLogin);
        message.setTimestamp(Instant.now());
        message.setType(ChatMessage.Type.JOINED);
        message.setContentCode("message.chat.joined.session");
        message.setContentArguments(userLogin);
        logMessage(session, message);

        return new JoinResult(session, message);
    }

    @Override
    public ChatMessage leaveSession(ChatSession session, String userLogin, ReasonForLeaving reason) {
        long id = session.getSessionId();
        pendingSessions.remove(id);

        if (sessions.remove(id) == null) {
            return null;
        }

        ChatMessage message = new ChatMessage();
        message.setUser(userLogin);
        message.setTimestamp(Instant.now());

        if (reason == ReasonForLeaving.ERROR) {
            message.setType(ChatMessage.Type.ERROR);
        } else {
            message.setType(ChatMessage.Type.LEFT);
        }

        switch (reason) {
            case ERROR:
                message.setContentCode("message.chat.left.chat.error");
                break;
            case LOGGED_OUT:
                message.setContentCode("message.chat.logged.out");
                break;
            default:
                message.setContentCode("message.chat.left.chat.normal");
                break;
        }
        message.setContentArguments(userLogin);
        logMessage(session, message);

        List<ChatMessage> chatLog = chatLogs.remove(id);
        try (FileOutputStream stream = new FileOutputStream("../chat-" + id + ".log")) {
            objectMapper.writeValue(stream, chatLog);
        } catch (IOException e) {
            log.error("Error while saving chat log to disk for session {}.", id);
        }

        return message;
    }

    @Override
    public void logMessage(ChatSession session, ChatMessage message) {
        List<ChatMessage> chatLog = chatLogs.get(session.getSessionId());
        if (chatLog == null) {
            log.warn("Attempt made to record chat message in non-existent log.");
        } else {
            chatLog.add(message);
        }
    }

    @Override
    public List<ChatSession> getPendingSessions() {
        return new ArrayList<>(pendingSessions.values());
    }

    private synchronized long getNextSessionId() {
        return SESSION_ID_SEQUENCE++;
    }

    @PostConstruct
    public void initialize() {
        objectMapper.addMixIn(ChatMessage.class, ChatMessage.MixInForLogWrite.class);
    }
}
