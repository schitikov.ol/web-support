package com.linefight.site.service.impl;

import com.linefight.site.entities.Attachment;
import com.linefight.site.entities.Ticket;
import com.linefight.site.entities.TicketComment;
import com.linefight.site.model.SearchResult;
import com.linefight.site.repository.AttachmentRepository;
import com.linefight.site.repository.TicketCommentRepository;
import com.linefight.site.repository.TicketRepository;
import com.linefight.site.service.TicketService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Optional;

@Service
public class DefaultTicketService implements TicketService {

    private final TicketRepository ticketRepository;
    private final AttachmentRepository attachmentRepository;
    private final TicketCommentRepository commentRepository;

    private static final Logger log = LogManager.getLogger();

    @Inject
    public DefaultTicketService(TicketRepository ticketRepository, AttachmentRepository attachmentRepository, TicketCommentRepository commentRepository) {
        this.ticketRepository = ticketRepository;
        this.attachmentRepository = attachmentRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    @Transactional
    public Page<SearchResult<Ticket>> search(String query, Pageable page, TicketExpand... expands) {
        log.debug("Searching with query: {}", query);

        Page<SearchResult<Ticket>> results = ticketRepository.search(query, page);

        results.forEach(r -> {
            for (TicketExpand e : expands) {
                e.accept(r.getEntity());
            }
        });

        return results;
    }

    @Override
    @Transactional
    public Page<Ticket> getAllTickets(Pageable page, TicketExpand... expands) {
        Page<Ticket> tickets = ticketRepository.findAll(page);
        tickets.forEach(t -> {
            for (TicketExpand e : expands) {
                e.accept(t);
            }
        });
        return tickets;
    }

    @Override
    @Transactional
    public Iterable<Ticket> getAllTickets(TicketExpand... expands) {
        Iterable<Ticket> tickets = ticketRepository.findAll();
        tickets.forEach(t -> {
            for (TicketExpand e : expands) {
                e.accept(t);
            }
        });
        return tickets;
    }

    @Override
    @Transactional
    public Ticket getTicket(long id) {
        Optional<Ticket> ticket = ticketRepository.findById(id);
        ticket.ifPresent(Ticket::getNumberOfAttachments);
        return ticket.orElse(null);
    }

    @Override
    @Transactional
    public void create(Ticket ticket) {
        ticket.setDateCreated(Instant.now());
        ticketRepository.save(ticket);
    }

    @Override
    @Transactional
    public void update(Ticket ticket) {
        ticketRepository.save(ticket);

    }

    @Override
    @Transactional
    public void deleteTicket(long id) {
        ticketRepository.deleteById(id);
    }

    @Override
    @Transactional
    public Page<TicketComment> getComments(long ticketId, Pageable page, CommentExpand... expands) {
        Page<TicketComment> comments = commentRepository.getByTicketId(ticketId, page);
        comments.forEach(c -> {
            for (CommentExpand e : expands)
                e.accept(c);
        });
        return comments;
    }

    @Override
    @Transactional
    public Iterable<TicketComment> getComments(long ticketId, CommentExpand... expands) {
        Iterable<TicketComment> comments = commentRepository.getByTicketId(ticketId);
        comments.forEach(c -> {
            for (CommentExpand e : expands)
                e.accept(c);
        });
        return comments;
    }

    @Override
    @Transactional
    public void create(TicketComment comment, long ticketId) {
        Optional<Ticket> ticket = ticketRepository.findById(ticketId);

        if (!ticket.isPresent())
            return;
        comment.setTicket(ticket.get());
        comment.setDateCreated(Instant.now());
        commentRepository.save(comment);
    }

    @Override
    @Transactional
    public void update(TicketComment comment) {
        commentRepository.save(comment);
    }

    @Override
    @Transactional
    public void deleteComment(long id) {
        commentRepository.deleteById(id);
    }

    @Override
    @Transactional
    public Attachment getAttachment(long id) {
        Optional<Attachment> attachment = attachmentRepository.findById(id);
        attachment.ifPresent(Attachment::getContents);

        return attachment.orElse(null);
    }
}
