package com.linefight.site.service.impl;

import com.linefight.site.entities.UserPrincipal;
import com.linefight.site.repository.UserRepository;
import com.linefight.site.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

@Service
public class DefaultUserService implements UserService {

    private static final Logger log = LogManager.getLogger();
    private static final SecureRandom RANDOM;
    private static final int HASHING_ROUNDS = 10;

    static {
        try {
            RANDOM = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        }
    }

    private final UserRepository userRepository;

    @Inject
    public DefaultUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserPrincipal loadUserByUsername(String username) throws UsernameNotFoundException {
        UserPrincipal principal = userRepository.getByLogin(username);

        if (principal == null)
            throw new UsernameNotFoundException("User with login \"" + username + "\" not found.");

        //make sure the authorities and password are loaded
        principal.getAuthorities().size();
        principal.getPassword();
        return principal;
    }

    @Override
    @Transactional
    public void saveUser(UserPrincipal principal, String newPassword) {
        String salt = BCrypt.gensalt(HASHING_ROUNDS, RANDOM);
        principal.setHashedPassword(BCrypt.hashpw(newPassword, salt).getBytes());

        userRepository.save(principal);
    }

    @Override
    @Transactional
    public void changeUserLocale(UserPrincipal principal, String newLocale) {
        principal.setLocale(newLocale);
        principal = userRepository.getByLogin(principal.getLogin());
        principal.setLocale(newLocale);

        userRepository.save(principal);
    }
}
