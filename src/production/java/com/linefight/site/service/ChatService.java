package com.linefight.site.service;

import com.linefight.site.chat.helper.CreateResult;
import com.linefight.site.chat.helper.JoinResult;
import com.linefight.site.chat.model.ChatMessage;
import com.linefight.site.chat.model.ChatSession;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;

import java.util.List;

public interface ChatService {

    @PreAuthorize("authentication.principal.login.equals(#userLogin) and hasAuthority('CREATE_CHAT_REQUEST') and (!#oauth2.OAuth or #oauth2.hasScope('write'))")
    CreateResult createSession(@P("userLogin") String userLogin);

    @PreAuthorize("authentication.principal.login.equals(#userLogin) and hasAuthority('START_CHAT') and (!#oauth2.OAuth or #oauth2.hasScope('write'))")
    JoinResult joinSession(long id, @P("userLogin") String userLogin);

    @PreAuthorize("authentication.principal.login.equals(#userLogin)" +
            " and (#userLogin.equals(#session.customerLogin) or #userLogin.equals(#session.representativeLogin))" +
            " and hasAuthority('CHAT') and (!#oauth2.OAuth or #oauth2.hasScope('write'))")
    ChatMessage leaveSession(@P("session") ChatSession session, @P("userLogin") String userLogin,
                             ReasonForLeaving reason);

    @PreAuthorize("authentication.principal.login.equals(#message.user)" +
            " and (#message.user.equals(#session.customerLogin) or #message.user.equals(#session.representativeLogin))" +
            " and hasAuthority('CHAT') and (!#oauth2.OAuth or #oauth2.hasScope('write'))")
    void logMessage(@P("session") ChatSession session, @P("message") ChatMessage message);

    @PreAuthorize("hasAuthority('VIEW_CHAT_REQUESTS') and (!#oauth2.OAuth or #oauth2.hasScope('read'))")
    List<ChatSession> getPendingSessions();

    enum ReasonForLeaving {
        NORMAL,
        LOGGED_OUT,
        ERROR
    }
}
