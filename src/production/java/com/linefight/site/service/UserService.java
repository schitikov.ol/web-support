package com.linefight.site.service;

import com.linefight.site.entities.UserPrincipal;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Validated
public interface UserService extends UserDetailsService {

    @Override
    UserPrincipal loadUserByUsername(String username) throws UsernameNotFoundException;

    void saveUser(@NotNull(message = "{validate.authenticate.principal") @Valid
                          UserPrincipal principal,
                  @NotBlank(message = "{validate.authenticate.password")
                          String newPassword
    );

    void changeUserLocale(@NotNull(message = "{validate.authenticate.principal}") @Valid
                                  UserPrincipal principal,
                          @NotBlank(message = "{validate.authenticate.locale}")
                                  String newLocale);
}
