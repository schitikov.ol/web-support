package com.linefight.site.service;

import com.linefight.site.entities.Attachment;
import com.linefight.site.entities.Ticket;
import com.linefight.site.entities.TicketComment;
import com.linefight.site.model.SearchResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.function.Consumer;

@Validated
public interface TicketService {
    @NotNull
    @PreAuthorize("hasAuthority('VIEW_TICKETS') and (!#oauth2.OAuth or #oauth2.hasScope('read'))")
    Page<Ticket> getAllTickets(@NotNull(message = "{validate.ticketService.getAllTickets.page}") Pageable page, TicketExpand... expands);

    @NotNull
    @PreAuthorize("hasAuthority('VIEW_TICKETS') and (!#oauth2.OAuth or #oauth2.hasScope('read'))")
    Iterable<Ticket> getAllTickets(TicketExpand... expands);

    @NotNull
    @PreAuthorize("hasAuthority('VIEW_TICKETS') and (!#oauth2.OAuth or #oauth2.hasScope('read'))")
    Page<SearchResult<Ticket>> search(
            @NotBlank(message = "{validate.ticketService.search.query}")
                    String query,
            @NotNull(message = "{validate.ticketService.search.page}")
                    Pageable page,
            TicketExpand... expands
    );

    @PreAuthorize("hasAuthority('VIEW_TICKET') and (!#oauth2.OAuth or #oauth2.hasScope('read'))")
    Ticket getTicket(@Positive(message = "{validate.ticketService.getTicket.id}") long id);

    @PreAuthorize("#ticket.id == 0 and hasAuthority('CREATE_TICKET') and (!#oauth2.OAuth or #oauth2.hasScope('write'))")
    void create(@NotNull(message = "{validate.ticketService.save.ticket}")
                @Valid @P("ticket") Ticket ticket);

    @PreAuthorize("((authentication.principal.equals(#ticket.customer) and hasAuthority('EDIT_OWN_TICKET'))" +
            " or hasAuthority('EDIT_ANY_TICKET')) and (!#oauth2.OAuth or #oauth2.hasScope('write'))")
    void update(@NotNull(message = "{validate.ticketService.save.ticket}")
                @Valid @P("ticket") Ticket ticket);

    @PreAuthorize("hasAuthority('DELETE_TICKET') and (!#oauth2.OAuth or #oauth2.hasScope('write'))")
    void deleteTicket(@Positive(message = "{validate.ticketService.getTicket.id}") long id);

    @PreAuthorize("hasAuthority('VIEW_COMMENTS') and (!#oauth2.OAuth or #oauth2.hasScope('read'))")
    Page<TicketComment> getComments(
            @Positive(message = "{validate.ticketService.getComments.id") long ticketId,
            @NotNull(message = "{validate.ticketService.getComments.page") Pageable page,
            CommentExpand... expands
    );

    @PreAuthorize("hasAuthority('VIEW_COMMENTS') and (!#oauth2.OAuth or #oauth2.hasScope('read'))")
    Iterable<TicketComment> getComments(
            @Positive(message = "{validate.ticketService.getComments.id") long ticketId,
            CommentExpand... expands
    );

    @PreAuthorize("#comment.id == 0 and hasAuthority('CREATE_COMMENT') and (!#oauth2.OAuth or #oauth2.hasScope('write'))")
    void create(@NotNull(message = "{validate.ticketService.save.comment}")
                @Valid @P("comment") TicketComment comment,
                @Positive(message = "{validate.ticketService.saveComment.id}")
                        long ticketId);

    @PreAuthorize("((authentication.principal.equals(comment.customer) and hasAuthority('EDIT_OWN_COMMENT'))" +
            " or hasAuthority('EDIT_ANY_COMMENT')) and (!#oauth2.OAuth or #oauth2.hasScope('write'))")
    void update(@NotNull(message = "{validate.ticketService.save.comment}")
                @Valid @P("comment") TicketComment comment);

    @PreAuthorize("hasAuthority('DELETE_COMMENT') and (!#oauth2.OAuth or #oauth2.hasScope('write'))")
    void deleteComment(@Positive(message = "{validate.ticketService.deleteComment.id}") long id);

    @PreAuthorize("hasAuthority('VIEW_ATTACHMENT') and (!#oauth2.OAuth or #oauth2.hasScope('read'))")
    Attachment getAttachment(@Positive(message = "{validate.ticketService.getAttachment.id}") long id);

    @FunctionalInterface
    interface TicketExpand extends Consumer<Ticket> {
    }

    @FunctionalInterface
    interface CommentExpand extends Consumer<TicketComment> {
    }
}
