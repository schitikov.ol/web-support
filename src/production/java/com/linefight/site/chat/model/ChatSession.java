package com.linefight.site.chat.model;

import javax.websocket.Session;
import java.util.Locale;
import java.util.function.BiConsumer;

public class ChatSession {
    private long sessionId;
    private String customerLogin;
    private Session customerWsSession;
    private String representativeLogin;
    private Session representativeWsSession;
    private BiConsumer<Session, Locale> onRepresentativeJoin;
    private ChatMessage creationMessage;

    public long getSessionId() {
        return sessionId;
    }

    public void setSessionId(long sessionId) {
        this.sessionId = sessionId;
    }

    public String getCustomerLogin() {
        return customerLogin;
    }

    public void setCustomerLogin(String customerLogin) {
        this.customerLogin = customerLogin;
    }

    public Session getCustomerWsSession() {
        return customerWsSession;
    }

    public void setCustomerWsSession(Session customerWsSession) {
        this.customerWsSession = customerWsSession;
    }

    public String getRepresentativeLogin() {
        return representativeLogin;
    }

    public void setRepresentativeLogin(String representativeLogin) {
        this.representativeLogin = representativeLogin;
    }

    public Session getRepresentativeWsSession() {
        return representativeWsSession;
    }

    public void setRepresentativeProperties(Session representativeWsSession, Locale representativeLocale) {
        this.representativeWsSession = representativeWsSession;
        if (onRepresentativeJoin != null) {
            onRepresentativeJoin.accept(representativeWsSession, representativeLocale);
        }
    }

    public void setOnRepresentativeJoin(BiConsumer<Session, Locale> onRepresentativeJoin) {
        this.onRepresentativeJoin = onRepresentativeJoin;
    }

    public ChatMessage getCreationMessage() {
        return creationMessage;
    }

    public void setCreationMessage(ChatMessage creationMessage) {
        this.creationMessage = creationMessage;
    }
}
