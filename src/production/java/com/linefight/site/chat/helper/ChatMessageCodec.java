package com.linefight.site.chat.helper;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.linefight.site.chat.model.ChatMessage;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.websocket.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class ChatMessageCodec implements
        Encoder.BinaryStream<ChatMessage>,
        Decoder.BinaryStream<ChatMessage> {
    private static final Logger log = LogManager.getLogger();
    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.findAndRegisterModules();
        MAPPER.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
        MAPPER.addMixIn(ChatMessage.class, ChatMessage.MixInForWebSocket.class);
    }

    @Override
    public ChatMessage decode(InputStream is) throws DecodeException, IOException {
        log.traceEntry();
        try {
            ChatMessage message = MAPPER.readValue(is, ChatMessage.class);
            message.setUserContent(URLDecoder.decode(message.getUserContent(), StandardCharsets.UTF_8.name()));
            return message;
        } catch (JsonParseException | JsonMappingException e) {
            throw new DecodeException((ByteBuffer) null, e.getMessage(), e);
        } finally {
            log.traceExit();
        }
    }

    @Override
    public void encode(ChatMessage chatMessage, OutputStream os) throws EncodeException, IOException {
        log.traceEntry();
        ChatMessage message = chatMessage.clone();
        try {
            if (StringUtils.isEmpty(message.getUserContent())) {
                message.setLocalizedContent(encodeMessageContent(message.getLocalizedContent()));
            } else {
                message.setUserContent(encodeMessageContent(message.getUserContent()));
            }
            os.write(MAPPER.writeValueAsBytes(message));
        } catch (JsonGenerationException | JsonMappingException e) {
            throw new EncodeException(chatMessage, e.getMessage(), e);
        } finally {
            log.traceExit();
        }
    }

    @Override
    public void init(EndpointConfig config) {

    }

    @Override
    public void destroy() {

    }

    private String encodeMessageContent(String messageContent) throws UnsupportedEncodingException {
        return URLEncoder.encode(messageContent, StandardCharsets.UTF_8.name())
                .replaceAll("\\+", "%20")
                .replaceAll("%21", "!")
                .replaceAll("%27", "'")
                .replaceAll("%28", "(")
                .replaceAll("%29", ")")
                .replaceAll("%7E", "~");
    }
}
