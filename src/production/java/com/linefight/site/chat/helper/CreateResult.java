package com.linefight.site.chat.helper;

import com.linefight.site.chat.model.ChatMessage;
import com.linefight.site.chat.model.ChatSession;

public class CreateResult {
    private final ChatSession chatSession;
    private final ChatMessage createMessage;

    public CreateResult(ChatSession chatSession, ChatMessage createMessage) {
        this.chatSession = chatSession;
        this.createMessage = createMessage;
    }

    public ChatSession getChatSession() {
        return chatSession;
    }

    public ChatMessage getCreateMessage() {
        return createMessage;
    }
}
