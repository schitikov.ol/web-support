package com.linefight.site.chat.helper;

import com.linefight.site.chat.model.ChatMessage;
import com.linefight.site.chat.model.ChatSession;

public class JoinResult {
    private final ChatSession chatSession;
    private final ChatMessage joinMessage;


    public JoinResult(ChatSession chatSession, ChatMessage joinMessage) {
        this.chatSession = chatSession;
        this.joinMessage = joinMessage;
    }

    public ChatSession getChatSession() {
        return chatSession;
    }

    public ChatMessage getJoinMessage() {
        return joinMessage;
    }
}
