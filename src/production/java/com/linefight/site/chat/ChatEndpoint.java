package com.linefight.site.chat;

import com.linefight.site.chat.helper.ChatMessageCodec;
import com.linefight.site.chat.helper.CreateResult;
import com.linefight.site.chat.helper.JoinResult;
import com.linefight.site.chat.model.ChatMessage;
import com.linefight.site.chat.model.ChatSession;
import com.linefight.site.entities.UserPrincipal;
import com.linefight.site.listener.SessionDestroyedListener;
import com.linefight.site.service.ChatService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.web.socket.server.standard.SpringConfigurator;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.server.ServerEndpointConfig;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ScheduledFuture;
import java.util.function.Consumer;


//TODO: make ChatEndpoint singleton
//For now, it has two instances of ChatEndpoint per chat session (for customer and for representative)
@ServerEndpoint(
        value = "/chat/{sessionId}",
        decoders = ChatMessageCodec.class,
        encoders = ChatMessageCodec.class,
        configurator = ChatEndpoint.EndpointConfigurator.class
)
public class ChatEndpoint {

    private static final Logger log = LogManager.getLogger();
    private static final byte[] pongData = "This is PONG country.".getBytes(StandardCharsets.UTF_8);

    private boolean closed = false;
    private Session wsSession;
    private Session otherWsSession;
    private HttpSession httpSession;
    private ChatSession chatSession;
    private final SessionDestroyedListener sessionDestroyedListener;
    private final ChatService chatService;
    private ScheduledFuture<?> pingFuture;
    private Locale locale;
    private Locale otherLocale;
    private final TaskScheduler taskScheduler;
    private final MessageSource messageSource;
    private UserPrincipal principal;
    private SecurityContext securityContext;

    private final Consumer<SessionDestroyedEvent> callback = this::httpSessionRemoved;

    @Inject
    public ChatEndpoint(SessionDestroyedListener sessionDestroyedListener, ChatService chatService, TaskScheduler taskScheduler, MessageSource messageSource) {
        this.sessionDestroyedListener = sessionDestroyedListener;
        this.chatService = chatService;
        this.taskScheduler = taskScheduler;
        this.messageSource = messageSource;
    }

    @OnOpen
    public void onOpen(Session wsSession, @PathParam("sessionId") long sessionId) {
        log.traceEntry(() -> sessionId);

        httpSession = EndpointConfigurator.getExposedSession(wsSession);
        locale = EndpointConfigurator.getExposedLocale(wsSession);
        securityContext = EndpointConfigurator.getExposedSecurityContext(wsSession);
        principal = (UserPrincipal) securityContext.getAuthentication().getPrincipal();

        doSecured(() -> {
            try {
                if (principal == null) {
                    log.warn("Attempt to access chat server while logged out.");
                    wsSession.close(new CloseReason(
                            CloseReason.CloseCodes.VIOLATED_POLICY,
                            messageSource.getMessage(
                                    "error.chat.not.logged.in", null, locale
                            )
                    ));
                    return;
                }

                if (sessionId < 1) {
                    //customer side of endpoint
                    CreateResult result =
                            chatService.createSession(principal.getLogin());
                    chatSession = result.getChatSession();
                    chatSession.setCustomerWsSession(wsSession);
                    chatSession.setOnRepresentativeJoin((s, l) -> {
                        otherWsSession = s;
                        otherLocale = l;
                    });
                    wsSession.getBasicRemote().sendObject(cloneAndLocalize(result.getCreateMessage(), locale));
                } else {
                    //representative side of endpoint
                    JoinResult result = chatService.joinSession(sessionId, principal.getLogin());
                    if (result == null) {
                        log.warn("Attempted to join non-existent chat session {}.", sessionId);
                        wsSession.close(new CloseReason(CloseReason.CloseCodes.UNEXPECTED_CONDITION,
                                messageSource.getMessage("error.chat.no.session", null, locale)));
                        return;
                    }
                    chatSession = result.getChatSession();
                    chatSession.setRepresentativeProperties(wsSession, locale);
                    otherWsSession = chatSession.getCustomerWsSession();
                    otherLocale = EndpointConfigurator.getExposedLocale(otherWsSession);

                    wsSession.getBasicRemote()
                            .sendObject(cloneAndLocalize(chatSession.getCreationMessage(), locale));
                    wsSession.getBasicRemote().sendObject(cloneAndLocalize(result.getJoinMessage(), locale));
                    otherWsSession.getBasicRemote().sendObject(cloneAndLocalize(result.getJoinMessage(), otherLocale));
                }

                this.wsSession = wsSession;
                log.debug("onOpen completed successfuly for chat {}.", sessionId);
            } catch (IOException | EncodeException e) {
                onError(e);
            } finally {
                log.traceExit();
            }
        });
    }

    @OnMessage
    public void onMessage(ChatMessage message) {
        doSecured(() -> {
            if (closed) {
                log.warn("Chat message received after connection closed.");
                return;
            }

            log.traceEntry();

            message.setUser(principal.getLogin());
            chatService.logMessage(chatSession, message);
            try {
                wsSession.getBasicRemote().sendObject(message);
                otherWsSession.getBasicRemote().sendObject(message);
            } catch (EncodeException | IOException e) {
                onError(e);
            }

            log.traceExit();
        });
    }

    @OnClose
    public void onClose(CloseReason reason) {
        doSecured(() -> {
            if (reason.getCloseCode() != CloseReason.CloseCodes.NORMAL_CLOSURE) {
                log.warn("Abnormal closure {} for reason [{}].", reason.getCloseCode(),
                        reason.getReasonPhrase());
            }

            //synchronizing on this because onClose and onError can be called simultaneously
            synchronized (this) {
                if (closed)
                    return;
                close(ChatService.ReasonForLeaving.NORMAL, null);
            }
        });
    }

    @OnError
    public void onError(Throwable e) {
        doSecured(() -> {
            log.warn("Error received in WebSocket session.", e);

            //synchronizing on this because onClose and onError can be called simultaneously
            synchronized (this) {
                if (closed)
                    return;
                close(ChatService.ReasonForLeaving.ERROR, "error.chat.closed.exception");
            }
        });
    }

    private void close(ChatService.ReasonForLeaving reason, String unexpected) {
        this.closed = true;
        if (!pingFuture.isCancelled())
            pingFuture.cancel(true);
        sessionDestroyedListener.deregisterOnRemoveCallback(callback);
        ChatMessage message = chatService.leaveSession(chatSession, principal.getLogin(), reason);

        if (message == null) {
            return;
        }

        CloseReason.CloseCodes closeCode;
        String messageCode = "message.chat.ended";
        if (reason == ChatService.ReasonForLeaving.ERROR)
            closeCode = CloseReason.CloseCodes.UNEXPECTED_CONDITION;
        else
            closeCode = CloseReason.CloseCodes.NORMAL_CLOSURE;

        //synchronizing because wsSession can be closing on the other side
        //noinspection SynchronizeOnNonFinalField
        synchronized (wsSession) {
            if (wsSession.isOpen()) {
                try {
                    wsSession.getBasicRemote().sendObject(cloneAndLocalize(message, locale));
                    wsSession.close(new CloseReason(closeCode,
                            messageSource.getMessage(messageCode, null, locale)));
                } catch (EncodeException | IOException e) {
                    log.error("Error closing chat connection.", e);
                }
            }
        }

        if (otherWsSession != null) {
            //synchronizing because otherWsSession can be closing on the other side
            //noinspection SynchronizeOnNonFinalField
            synchronized (otherWsSession) {
                if (otherWsSession.isOpen()) {
                    try {
                        otherWsSession.getBasicRemote().sendObject(cloneAndLocalize(message, otherLocale));
                        otherWsSession.close(new CloseReason(closeCode,
                                messageSource.getMessage(messageCode, null, otherLocale)));
                    } catch (EncodeException | IOException e) {
                        log.error("Error closing companion chat connection.");
                    }
                }
            }
        }
    }

    private void sendPing() {
        if (!wsSession.isOpen()) {
            return;
        }
        log.debug("Sending ping to WebSocket client.");

        try {
            wsSession.getBasicRemote()
                    .sendPing(ByteBuffer.wrap(pongData));
        } catch (IOException e) {
            log.warn("Failed to send ping message to WebSocket client.", e);
        }
    }

    @OnMessage
    public void onPong(PongMessage message) {
        ByteBuffer data = message.getApplicationData();
        if (!Arrays.equals(pongData, data.array()))
            log.warn("Received pong message with incorrect payload.");
        else
            log.debug("Received good pong message.");
    }

    @PostConstruct
    public void initialize() {
        sessionDestroyedListener.registerOnRemoveCallback(callback);

        pingFuture = taskScheduler.scheduleWithFixedDelay(
                this::sendPing,
                new Date(System.currentTimeMillis() + 25_000L),
                25_000L);
    }

    private void httpSessionRemoved(SessionDestroyedEvent event) {
        if (event.getId().equals(this.httpSession.getId())) {
            synchronized (this) {
                if (closed)
                    return;
                log.info("Chat session ended abruptly by {} logging out.",
                        principal.getLogin());
                close(ChatService.ReasonForLeaving.LOGGED_OUT, null);
            }
        }
    }

    private ChatMessage cloneAndLocalize(ChatMessage message, Locale locale) {
        message = message.clone();
        message.setLocalizedContent(messageSource.getMessage(
                message.getContentCode(), message.getContentArguments(), locale
        ));
        return message;
    }

    private void doSecured(SecuredAction securedAction) {
        SecurityContextHolder.setContext(securityContext);

        try {
            securedAction.execute();
        } finally {
            SecurityContextHolder.clearContext();
        }
    }

    @FunctionalInterface
    private interface SecuredAction {
        void execute();
    }

    public static class EndpointConfigurator extends SpringConfigurator {

        private static final String HTTP_SESSION_KEY = "com.linefight.ws.http.session";
        private static final String SECURITY_CONTEXT_KEY = "com.linefight.ws.security.context";
        private static final String LOCALE_KEY = "com.linefight.ws.user.locale";

        private static HttpSession getExposedSession(Session wsSession) {
            return (HttpSession) wsSession.getUserProperties().get(HTTP_SESSION_KEY);
        }

        private static SecurityContext getExposedSecurityContext(Session wsSession) {
            return (SecurityContext) wsSession.getUserProperties().get(SECURITY_CONTEXT_KEY);
        }

        private static Locale getExposedLocale(Session wsSession) {
            return (Locale) wsSession.getUserProperties().get(LOCALE_KEY);
        }

        @Override
        public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
            log.traceEntry();
            super.modifyHandshake(sec, request, response);

            HttpSession httpSession = (HttpSession) request.getHttpSession();
            UserPrincipal principal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            sec.getUserProperties().put(HTTP_SESSION_KEY, httpSession);
            sec.getUserProperties()
                    .put(SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());
            sec.getUserProperties()
                    .put(LOCALE_KEY, Locale.forLanguageTag(principal.getLocale()));
            log.traceExit();
        }
    }
}
