package com.linefight.site.entities;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.linefight.site.converter.InstantConverter;
import com.linefight.site.converter.InstantXmlAdapter;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Field;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TicketComment")
@XmlRootElement(namespace = "http://linefight.com/xmlns/support", name = "comment")
@XmlAccessorType(XmlAccessType.NONE)
@JsonAutoDetect(creatorVisibility = JsonAutoDetect.Visibility.NONE,
        fieldVisibility = JsonAutoDetect.Visibility.NONE,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        isGetterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class TicketComment implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CommentId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlElement
    @JsonProperty
    private long id;

    @NotNull(message = "{validate.ticket.comment.customer}")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "UserId")
    @XmlElement
    @JsonProperty
    private UserPrincipal customer;

    @NotBlank(message = "{validate.ticket.comment.body}")
    @Lob
    @XmlElement
    @JsonProperty
    @Analyzer(definition = "customanalyzer")
    @Field
    private String body;

    @Convert(converter = InstantConverter.class)
    @XmlElement
    @XmlJavaTypeAdapter(InstantXmlAdapter.class)
    @JsonProperty
    private Instant dateCreated;

    @Valid
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JoinTable(name = "TicketComment_Attachment",
            joinColumns = {@JoinColumn(name = "CommentId")},
            inverseJoinColumns = {@JoinColumn(name = "AttachmentId")})
    @OrderColumn(name = "SortKey")
    @XmlElement(name = "attachment")
    @JsonProperty
    private List<Attachment> attachments = new ArrayList<>();

    @Valid
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "TicketId")
    @ContainedIn
    private Ticket ticket;

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserPrincipal getCustomer() {
        return this.customer;
    }

    public void setCustomer(UserPrincipal customer) {
        this.customer = customer;
    }

    public String getBody() {
        return this.body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Instant getDateCreated() {
        return this.dateCreated;
    }

    public void setDateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
    }

    public List<Attachment> getAttachments() {
        return this.attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public void addAttachment(Attachment attachment) {
        this.attachments.add(attachment);
    }

    public int getNumberOfAttachments() {
        return this.attachments.size();
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
}
