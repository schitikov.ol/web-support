package com.linefight.site.entities;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement(namespace = "http://linefight.com/xmlns/support", name = "attachment")
@XmlAccessorType(XmlAccessType.NONE)
@JsonAutoDetect(creatorVisibility = JsonAutoDetect.Visibility.NONE,
        fieldVisibility = JsonAutoDetect.Visibility.NONE,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        isGetterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
@Entity
public class Attachment implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "AttachmentId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlElement
    @JsonProperty
    private long id;

    @NotBlank(message = "{validate.attachment.name}")
    @Basic
    @Column(name = "AttachmentName")
    @XmlElement
    @JsonProperty
    private String name;

    @Size(min = 1, message = "{validate.attachments.contents}")
    @XmlSchemaType(name = "base64Binary")
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @XmlElement
    @JsonProperty
    private byte[] contents;

    @NotBlank(message = "{validate.attachment.mimeContentType}")
    @Basic
    @XmlElement
    @JsonProperty
    private String contentType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getContents() {
        return contents;
    }

    public void setContents(byte[] contents) {
        this.contents = contents;
    }

    public long getContentLength() {
        return contents.length;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
