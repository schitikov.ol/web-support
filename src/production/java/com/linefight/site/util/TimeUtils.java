package com.linefight.site.util;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Locale;

@SuppressWarnings("AccessStaticViaInstance")
@Component
public final class TimeUtils {

    private static MessageSource messageSource;

    @Inject
    public TimeUtils(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public static String intervalToString(long timeInterval) {
        Locale locale = LocaleContextHolder.getLocale();
        if (timeInterval < 1_000)
            return messageSource.getMessage("message.sessionList.lessThanSecond", null, locale);
        if (timeInterval < 60_000)
            return messageSource.getMessage("message.sessionList.seconds", new Object[]{timeInterval / 1000}, locale);
        return messageSource.getMessage("message.sessionList.aboutMinutes", new Object[]{timeInterval / 60_000}, locale);
    }
}