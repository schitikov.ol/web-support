package com.linefight.config;

import com.linefight.site.controller.soap.TicketSoapEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.web.context.support.ServletContextResource;
import org.springframework.ws.WebServiceMessageFactory;
import org.springframework.ws.config.annotation.WsConfigurationSupport;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.server.endpoint.adapter.DefaultMethodEndpointAdapter;
import org.springframework.ws.server.endpoint.adapter.method.MarshallingPayloadMethodProcessor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.soap.SoapVersion;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;
import org.springframework.ws.soap.server.endpoint.interceptor.PayloadValidatingInterceptor;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import java.util.List;

@Configuration
@ComponentScan(
        basePackages = "com.linefight.site",
        useDefaultFilters = false,
        includeFilters = @ComponentScan.Filter(Endpoint.class)
)
public class SoapServletContextConfiguration extends WsConfigurationSupport {

    private final ServletContext servletContext;

    private final Marshaller marshaller;

    private final Unmarshaller unmarshaller;

    @Inject
    public SoapServletContextConfiguration(ServletContext servletContext, Marshaller marshaller, Unmarshaller unmarshaller) {
        this.servletContext = servletContext;
        this.marshaller = marshaller;
        this.unmarshaller = unmarshaller;
    }


    @Bean
    public WebServiceMessageFactory messageFactory() {
        SaajSoapMessageFactory factory = new SaajSoapMessageFactory();
        factory.setSoapVersion(SoapVersion.SOAP_12);
        return factory;
    }

    @Bean
    public DefaultWsdl11Definition defaultWsdl11Definition() {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("Support");
        wsdl11Definition.setLocationUri("/services/soap/");
        wsdl11Definition.setCreateSoap11Binding(false);
        wsdl11Definition.setCreateSoap12Binding(true);
        wsdl11Definition.setTargetNamespace(TicketSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(supportSchema());
        return wsdl11Definition;
    }


    @Bean
    public XsdSchema supportSchema() {
        return new SimpleXsdSchema(new ServletContextResource(servletContext, "/WEB-INF/xsd/soap/support.xsd"));
    }

    @Bean
    public MarshallingPayloadMethodProcessor methodProcessor() {
        return new MarshallingPayloadMethodProcessor(marshaller, unmarshaller);
    }

    @Override
    public void addInterceptors(List<EndpointInterceptor> interceptors) {
        PayloadValidatingInterceptor validatingInterceptor = new PayloadValidatingInterceptor();
        validatingInterceptor.setValidateRequest(true);
        validatingInterceptor.setValidateResponse(false);
        validatingInterceptor.setXsdSchema(supportSchema());
        interceptors.add(validatingInterceptor);
    }

    //add MarshallingPayloadMethodProcessor before default ones
    @PostConstruct
    private void addMarshallingPayloadMethodProcessor() {
        DefaultMethodEndpointAdapter adapter = defaultMethodEndpointAdapter();
        adapter.getMethodArgumentResolvers().add(0, methodProcessor());
        adapter.getMethodReturnValueHandlers().add(0, methodProcessor());
    }
}
