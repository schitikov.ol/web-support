package com.linefight.config;

import com.linefight.site.entities.UserPrincipal;
import com.linefight.site.service.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

public class UserPrincipalLocaleResolverDecorator implements LocaleResolver {
    private LocaleResolver localeResolver;
    private UserService userService;

    public UserPrincipalLocaleResolverDecorator(LocaleResolver localeResolver, UserService userService) {
        this.localeResolver = localeResolver;
        this.userService = userService;
    }

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        String locale = null;

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserPrincipal) {
            locale = ((UserPrincipal) principal).getLocale();
        }

        if (locale == null) {
            locale = localeResolver.resolveLocale(request).toLanguageTag();
            if (principal instanceof UserPrincipal) {
                userService.changeUserLocale((UserPrincipal) principal, locale);
            }
        }

        return Locale.forLanguageTag(locale);
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserPrincipal) {
            userService.changeUserLocale((UserPrincipal) principal, locale.toLanguageTag());
        }

        localeResolver.setLocale(request, response, locale);
    }
}
