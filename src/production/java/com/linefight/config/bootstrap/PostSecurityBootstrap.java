package com.linefight.config.bootstrap;

import com.linefight.site.filter.PostSecurityLoggingFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.web.WebApplicationInitializer;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;

@Order(3)
public class PostSecurityBootstrap implements WebApplicationInitializer {

    private static final Logger log = LogManager.getLogger();

    @Override
    public void onStartup(ServletContext servletContext) {

        log.info("Executing post security bootstrap.");

        FilterRegistration.Dynamic registration = servletContext.addFilter("postSecurityLoggingFilter", new PostSecurityLoggingFilter());
        registration.addMappingForUrlPatterns(null, false, "/*");

    }
}
