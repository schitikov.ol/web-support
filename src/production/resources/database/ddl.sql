CREATE TABLE UserPrincipal (
  UserId                BIGINT                NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Login                 VARCHAR(30) UNIQUE    NOT NULL,
  HashedPassword        BINARY(60)            NOT NULL,
  Locale                VARCHAR(30)           NOT NULL,
  AccountNonExpired     BOOLEAN               NOT NULL,
  AccountNonLocked      BOOLEAN               NOT NULL,
  CredentialsNonExpired BOOLEAN               NOT NULL,
  Enabled               BOOLEAN               NOT NULL,
);

CREATE TABLE UserPrincipal_Authority (
  UserId    BIGINT       NOT NULL,
  Authority VARCHAR(100) NOT NULL,
  CONSTRAINT UserPrincipal_Authority_User_Authority UNIQUE (UserId, Authority),
  CONSTRAINT UserPrincipal_Authority_UserId FOREIGN KEY (UserId)
  REFERENCES UserPrincipal (UserId)
  ON DELETE CASCADE

);

CREATE TABLE Ticket (
  TicketId    BIGINT       NOT NULL AUTO_INCREMENT PRIMARY KEY,
  UserId      BIGINT       NOT NULL,
  Subject     VARCHAR(255) NOT NULL,
  Body        TEXT,
  DateCreated TIMESTAMP(6) NULL,
  CONSTRAINT Ticket_UserId FOREIGN KEY (UserId)
  REFERENCES UserPrincipal (UserId)
  ON DELETE CASCADE
);

CREATE TABLE TicketComment (
  CommentId   BIGINT       NOT NULL AUTO_INCREMENT PRIMARY KEY,
  TicketId    BIGINT       NOT NULL,
  UserId      BIGINT       NOT NULL,
  Body        TEXT,
  DateCreated TIMESTAMP(6) NULL,
  CONSTRAINT TicketComment_UserId FOREIGN KEY (UserId)
  REFERENCES UserPrincipal (UserId)
  ON DELETE CASCADE,
  CONSTRAINT TicketComment_TicketId FOREIGN KEY (TicketId)
  REFERENCES Ticket (TicketId)
  ON DELETE CASCADE
);

CREATE TABLE Attachment (
  AttachmentId   BIGINT       NOT NULL AUTO_INCREMENT PRIMARY KEY,
  AttachmentName VARCHAR(255) NULL,
  ContentType    VARCHAR(255) NOT NULL,
  Contents       MEDIUMBLOB   NOT NULL
);

CREATE TABLE Ticket_Attachment (
  SortKey      SMALLINT NOT NULL,
  TicketId     BIGINT   NOT NULL,
  AttachmentId BIGINT   NOT NULL,
  CONSTRAINT Ticket_Attachment_Ticket FOREIGN KEY (TicketId)
  REFERENCES Ticket (TicketId)
  ON DELETE CASCADE,
  CONSTRAINT Ticket_Attachment_Attachment FOREIGN KEY (AttachmentId)
  REFERENCES Attachment (AttachmentId)
  ON DELETE CASCADE,
);

CREATE TABLE TicketComment_Attachment (
  SortKey      SMALLINT NOT NULL,
  CommentId    BIGINT   NOT NULL,
  AttachmentId BIGINT   NOT NULL,
  CONSTRAINT TicketComment_Attachment_Comment FOREIGN KEY (CommentId)
  REFERENCES TicketComment (CommentId)
  ON DELETE CASCADE,
  CONSTRAINT TicketComment_Attachment_Attachment FOREIGN KEY (AttachmentId)
  REFERENCES Attachment (AttachmentId)
  ON DELETE CASCADE,
);

create table oauth_client_details (
  client_id               VARCHAR(256) PRIMARY KEY,
  resource_ids            VARCHAR(256),
  client_secret           VARCHAR(256),
  scope                   VARCHAR(256),
  authorized_grant_types  VARCHAR(256),
  web_server_redirect_uri VARCHAR(256),
  authorities             VARCHAR(256),
  access_token_validity   INTEGER,
  refresh_token_validity  INTEGER,
  additional_information  VARCHAR(4096),
  autoapprove             VARCHAR(256)
);

create table oauth_client_token (
  token_id          VARCHAR(256),
  token             BLOB,
  authentication_id VARCHAR(256) PRIMARY KEY,
  user_name         VARCHAR(256),
  client_id         VARCHAR(256)
);

create table oauth_access_token (
  token_id          VARCHAR(256),
  token             BLOB,
  authentication_id VARCHAR(256) PRIMARY KEY,
  user_name         VARCHAR(256),
  client_id         VARCHAR(256),
  authentication    BLOB,
  refresh_token     VARCHAR(256)
);

create table oauth_refresh_token (
  token_id       VARCHAR(256),
  token          BLOB,
  authentication BLOB
);

create table oauth_code (
  code           VARCHAR(256),
  authentication BLOB
);

create table oauth_approvals (
  userId         VARCHAR(256),
  clientId       VARCHAR(256),
  scope          VARCHAR(256),
  status         VARCHAR(10),
  expiresAt      TIMESTAMP,
  lastModifiedAt TIMESTAMP
);

INSERT INTO UserPrincipal (Login,
                           HashedPassword,
                           Locale,
                           AccountNonExpired,
                           AccountNonLocked,
                           CredentialsNonExpired,
                           Enabled)
VALUES (-- password
        'Bob',
        STRINGTOUTF8('$2a$10$x0k/yA5qN8SP8JD5CEN.6elEBFxVVHeKZTdyv.RPra4jzRR5SlKSC'),
        'en-US',
        TRUE,
        TRUE,
        TRUE,
        TRUE);

INSERT INTO UserPrincipal_Authority (UserId, Authority)
VALUES (1, 'VIEW_TICKETS'),
       (1, 'VIEW_TICKET'),
       (1, 'CREATE_TICKET'),
       (1, 'EDIT_OWN_TICKET'),
       (1, 'VIEW_COMMENTS'),
       (1, 'CREATE_COMMENT'),
       (1, 'EDIT_OWN_COMMENT'),
       (1, 'VIEW_ATTACHMENT'),
       (1, 'CREATE_CHAT_REQUEST'),
       (1, 'CHAT');

INSERT INTO UserPrincipal (Login,
                           HashedPassword,
                           Locale,
                           AccountNonExpired,
                           AccountNonLocked,
                           CredentialsNonExpired,
                           Enabled)
VALUES (-- drowssap
        'Peter',
        STRINGTOUTF8('$2a$10$JSxmYO.JOb4TT42/4RFzguaTuYkZLCfeND1bB0rzoy7wH0RQFEq8y'),
        'en-US',
        TRUE,
        TRUE,
        TRUE,
        TRUE);

INSERT INTO UserPrincipal_Authority (UserId, Authority)
VALUES (2, 'VIEW_TICKETS'),
       (2, 'VIEW_TICKET'),
       (2, 'CREATE_TICKET'),
       (2, 'EDIT_OWN_TICKET'),
       (2, 'VIEW_COMMENTS'),
       (2, 'CREATE_COMMENT'),
       (2, 'EDIT_OWN_COMMENT'),
       (2, 'VIEW_ATTACHMENT'),
       (2, 'CREATE_CHAT_REQUEST'),
       (2, 'CHAT');

INSERT INTO UserPrincipal (Login,
                           HashedPassword,
                           Locale,
                           AccountNonExpired,
                           AccountNonLocked,
                           CredentialsNonExpired,
                           Enabled)
VALUES (-- wordpass
        'Mike',
        STRINGTOUTF8('$2a$10$Lc0W6stzND.9YnFRcfbOt.EaCVO9aJ/QpbWnfjJLcMovdTx5s4i3G'),
        'en-US',
        TRUE,
        TRUE,
        TRUE,
        TRUE);
INSERT INTO UserPrincipal_Authority (UserId, Authority)
VALUES (3, 'VIEW_TICKETS'),
       (3, 'VIEW_TICKET'),
       (3, 'CREATE_TICKET'),
       (3, 'EDIT_OWN_TICKET'),
       (3, 'VIEW_COMMENTS'),
       (3, 'CREATE_COMMENT'),
       (3, 'EDIT_OWN_COMMENT'),
       (3, 'VIEW_ATTACHMENT'),
       (3, 'CREATE_CHAT_REQUEST'),
       (3, 'CHAT');

INSERT INTO UserPrincipal (Login,
                           HashedPassword,
                           Locale,
                           AccountNonExpired,
                           AccountNonLocked,
                           CredentialsNonExpired,
                           Enabled)
VALUES (-- green
        'John',
        STRINGTOUTF8('$2a$10$vacuqbDw9I7rr6RRH8sByuktOzqTheQMfnK3XCT2WlaL7vt/3AMby'),
        'en-US',
        TRUE,
        TRUE,
        TRUE,
        TRUE);

INSERT INTO UserPrincipal_Authority (UserId, Authority)
VALUES (4, 'VIEW_TICKETS'),
       (4, 'VIEW_TICKET'),
       (4, 'CREATE_TICKET'),
       (4, 'EDIT_OWN_TICKET'),
       (4, 'VIEW_COMMENTS'),
       (4, 'CREATE_COMMENT'),
       (4, 'EDIT_OWN_COMMENT'),
       (4, 'VIEW_ATTACHMENT'),
       (4, 'CREATE_CHAT_REQUEST'),
       (4, 'CHAT'),
       (4, 'EDIT_ANY_TICKET'),
       (4, 'DELETE_TICKET'),
       (4, 'EDIT_ANY_COMMENT'),
       (4, 'DELETE_COMMENT'),
       (4, 'VIEW_USER_SESSIONS'),
       (4, 'VIEW_CHAT_REQUESTS'),
       (4, 'START_CHAT');

INSERT INTO UserPrincipal_Authority (UserId, Authority)
VALUES (1, 'USE_WEB_SERVICES'),
       (4, 'USE_WEB_SERVICES');

INSERT INTO OAUTH_CLIENT_DETAILS (CLIENT_ID,
                                  RESOURCE_IDS,
                                  CLIENT_SECRET,
                                  SCOPE, AUTHORIZED_GRANT_TYPES, AUTHORITIES, web_server_redirect_uri,
                                  ACCESS_TOKEN_VALIDITY,
                                  REFRESH_TOKEN_VALIDITY)
VALUES ('oauth-client',
        'support',
        /*password*/
        '$2a$10$102iEZ.XN9eVeL1Rc67NEOd2Ugzjb.YExa/pvZP2l5E549AcKbcLy',
        'read,write', 'authorization_code', 'OAUTH-CLIENT', 'http://localhost:8080/',
        10800,
        2592000);

