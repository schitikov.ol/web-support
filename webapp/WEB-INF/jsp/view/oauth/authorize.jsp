<%--@elvariable id="_csrf" type="org.springframework.security.web.csrf.CsrfToken"--%>
<%--@elvariable id="authorizationRequest" type="org.springframework.security.oauth2.provider.AuthorizationRequest"--%>
<%--@elvariable id="scopes" type="java.util.Map<java.lang.String, java.lang.String>"--%>
<spring:message code="title.oauthAuthorize" var="oauthAuthorize"/>
<template:basic htmlTitle="${oauthAuthorize}" bodyTitle="${oauthAuthorize}">
    <div class="container">
        <p>
            <spring:message code="oauth.authorizeThirdParty">
                <spring:argument value="${authorizationRequest.clientId}"/>
            </spring:message>
        </p>
        <form class="d-sm-inline-block" id="confirmationForm" name="confirmationForm" method="post"
              action="<c:url value="/oauth/authorize" />">
            <input type="hidden" name="${_csrf.parameterName}"
                   value="${_csrf.token}"/>
            <input type="hidden" name="user_oauth_approval" value="true"/>
            <c:forEach items="${scopes.keySet()}" var="scope">
                <input type="hidden" name="${scope}" value="true"/>
            </c:forEach>
            <button type="submit" name="authorize" value="Authorize"
                    class="btn btn-primary"><spring:message code="oauth.authorizeApprove"/></button>
        </form>
        <form class="d-sm-inline-block" id="denialForm" name="denialForm" method="post"
              action="<c:url value="/oauth/authorize" />">
            <input type="hidden" name="${_csrf.parameterName}"
                   value="${_csrf.token}"/>
            <input type="hidden" name="user_oauth_approval" value="false"/>
            <c:forEach items="${scopes.keySet()}" var="scope">
                <input type="hidden" name="${scope}" value="false"/>
            </c:forEach>
            <button type="submit" name="deny" value="Deny"
                    class="btn btn-danger"><spring:message code="oauth.authorizeDeny"/></button>
        </form>
    </div>
</template:basic>