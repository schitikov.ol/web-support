<%--@elvariable id="validationErrors" type="java.util.Set<javax.validation.ConstraintViolation>"--%>
<spring:message code="title.ticketAdd" var="addTitle"/>
<template:basic htmlTitle="${addTitle}" bodyTitle="${addTitle}">
    <div class="container w-25" style="min-width: 300px">
            <%--@elvariable id="ticketForm" type="com.linefight.site.controller.web.main.TicketController.TicketForm"--%>
        <form:form method="post" enctype="multipart/form-data" modelAttribute="ticketForm">
            <div class="form-group">
                <form:label path="subject" for="subject"><spring:message code="field.ticket.subject"/></form:label>
                <form:input path="subject" id="subject" cssClass="form-control"/>
                <form:errors path="subject" cssClass="d-block alert alert-danger mt-2"/>
            </div>
            <div class="form-group">
                <form:label path="body" for="body"><spring:message code="field.ticket.body"/></form:label>
                <form:textarea path="body" id="body" cssClass="form-control" rows="5" cols="30"/>
                <form:errors path="body" cssClass="d-block alert alert-danger mt-2"/>
            </div>
            <div class="form-group">
                <label for="file"><b><spring:message code="field.ticket.attachments"/></b></label>
                <input type="file" id="file" name="attachments" multiple="multiple" class="form-control-file"/>
                <form:errors path="attachments" cssClass="d-block alert alert-danger mt-2"/>
            </div>
            <input type="submit" value="<spring:message code="button.ticket.submit"/>" class="btn btn-primary"/>
        </form:form>
        <c:if test="${validationErrors != null}">
            <ul class="list-group mt-2">
                <c:forEach items="${validationErrors}" var="error">
                    <li class="list-group-item list-group-item-danger">
                        <c:out value="${error.message}"/>
                    </li>
                </c:forEach>
            </ul>
        </c:if>
    </div>
</template:basic>
