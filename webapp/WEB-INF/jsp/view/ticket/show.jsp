<%--@elvariable id="ticket" type="com.linefight.site.entities.Ticket"--%>
<%--@elvariable id="ticketId" type="java.lang.String"--%>
<%--@elvariable id="comments" type="org.springframework.data.domain.Page<com.linefight.site.model.TicketComment>"--%>
<spring:message code="title.ticketShow" var="showTitle"/>
<template:basic htmlTitle="${ticket.subject}"
                bodyTitle="${showTitle} #${ticketId}: ${ticket.subject}">
    <div class="container">
        <i><spring:message code="message.ticketShow.customerLogin"/> -
            <c:out value="${ticket.customer.login}"/><br/>
            <spring:message code="message.ticketShow.created"/>&nbsp;
            <utils:formatDate value="${ticket.dateCreated}" type="both"
                              timeStyle="long" dateStyle="full"/></i>
    </div>
    <br/>
    <div class="container">
        <p><c:out value="${ticket.body}"/></p>
    </div>

    <c:if test="${ticket.numberOfAttachments > 0}">
        <div class="container">
            <spring:message code="message.ticketShow.attachments"/>:
            <c:forEach items="${ticket.attachments}" var="attachment"
                       varStatus="status">
                <c:if test="${!status.first}">, </c:if>
                <a href="<c:url value="/ticket/attachment/${attachment.id}"/>">
                    <c:out value="${attachment.name}"/></a>
            </c:forEach>
        </div>
    </c:if>

    <div class="container mt-2">
        <h3><spring:message code="title.ticketShow.comments"/></h3>
        <c:choose>
            <c:when test="${comments.totalElements == 0}">
                <i><spring:message code="message.ticketShow.noComments"/></i><br/><br/>
            </c:when>
            <c:otherwise>
                <c:forEach items="${comments.content}" var="comment">
                    <i><c:out value="${comment.customer.login}"/>&nbsp;</i>
                    (<utils:formatDate value="${comment.dateCreated}" type="both"
                                       timeStyle="short" dateStyle="medium"/>)<br/>
                    <c:out value="${comment.body}"/><br/>
                    <c:if test="${comment.numberOfAttachments > 0}">
                        <spring:message code="message.ticketShow.attachments"/>:
                        <c:forEach items="${comment.attachments}" var="attachment"
                                   varStatus="status">
                            <c:if test="${!status.first}">, </c:if>
                            <a href="<c:url value="/ticket/attachment/${attachment.id}"/>">
                                <c:out value="${attachment.name}"/></a>
                        </c:forEach>
                    </c:if><br/><br/>
                </c:forEach>
            </c:otherwise>
        </c:choose>
    </div>

    <div class="container">
        <b><spring:message code="message.ticketShow.addComment"/></b>
        <c:url value="/ticket/comment/${ticketId}" var="formAction">
            <c:param name="paging.page" value="${comments.number + 1}"/>
            <c:param name="paging.size" value="10"/>
        </c:url>
            <%--@elvariable id="commentForm" type="com.linefight.site.controller.web.main.TicketController.CommentForm"--%>
        <form:form action="${formAction}" enctype="multipart/form-data" method="post" modelAttribute="commentForm">
            <div class="form-group">
                <form:label path="body"><spring:message code="field.ticket.comment.body"/></form:label>
                <form:textarea path="body" rows="5" cols="30" cssClass="form-control w-50"/>
                <form:errors path="body" cssClass="d-block alert alert-danger mt-2 w-50"/>
            </div>
            <div class="form-group w-50">
                <label for="file"><b><spring:message code="field.ticket.attachments"/></b></label>
                <input type="file" id="file" name="attachments" multiple="multiple" class="form-control-file"/>
                <form:errors path="attachments" cssClass="d-block alert alert-danger mt-2"/>
            </div>
            <input type="submit" value="<spring:message code="button.ticket.submit"/>" class="btn btn-primary"/>
        </form:form>
    </div>

    <c:if test="${comments.totalPages > 1}">
        <div class="container mt-2">
            <ul class="pagination justify-content-center">
                <c:set value="" var="disabledPrevious"/>
                <c:set value="" var="disabledNext"/>
                <c:if test="${comments.first}">
                    <c:set value="disabled" var="disabledPrevious"/>
                </c:if>
                <c:if test="${comments.last}">
                    <c:set value="disabled" var="disabledNext"/>
                </c:if>

                <li class="page-item ${disabledPrevious}">
                    <a class="page-link" href="<c:url value="/ticket/show/${ticketId}">
                                                <c:param name="paging.page" value="${comments.number}"/>
                                                <c:param name="paging.size" value="10"/>
                                           </c:url>">&laquo;</a>
                </li>
                <c:forEach begin="1" end="${comments.totalPages}" var="i">
                    <c:set value="" var="pageActive"/>
                    <c:if test="${(i - 1) == comments.number}">
                        <c:set value="active" var="pageActive"/>
                    </c:if>
                    <li class="page-item ${pageActive}">
                        <a class="page-link" href="<c:url value="/ticket/show/${ticketId}">
                                                <c:param name="paging.page" value="${i}"/>
                                                <c:param name="paging.size" value="10"/>
                                           </c:url>">${i}</a>
                    </li>
                </c:forEach>
                <li class="page-item ${disabledNext}">
                    <a class="page-link" href="<c:url value="/ticket/show/${ticketId}">
                                                <c:param name="paging.page" value="${comments.number + 2}"/>
                                                <c:param name="paging.size" value="10"/>
                                           </c:url>">&raquo;</a>
                </li>
            </ul>
        </div>
    </c:if>

</template:basic>
