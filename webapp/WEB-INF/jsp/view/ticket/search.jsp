<%--@elvariable id="results" type="org.springframework.data.domain.Page<com.linefight.site.model.SearchResult<com.linefight.site.entities.Ticket>>"--%>
<spring:message code="title.ticketSearch" var="title"/>
<template:basic htmlTitle="${title}" bodyTitle="${title}">
    <c:choose>
        <c:when test="${results.totalElements == 0}">
            <div class="container mt-2">
                <i><spring:message code="message.ticketSearch.none"/></i>
            </div>
        </c:when>
        <c:otherwise>
            <c:forEach items="${results.content}" var="result">
                <div class="container mt-2">
                    <spring:message code="message.ticketList.ticket"/>&nbsp;${result.entity.id}:
                    <a href="<c:url value="/ticket/show/${result.entity.id}"/>">
                        <c:out value="${utils:abbreviateString(result.entity.subject, 60)}"/>
                    </a>
                    <c:out value="${result.entity.customer.login}"/>&nbsp;<spring:message
                        code="message.ticketList.created"/>&nbsp;
                    <utils:formatDate value="${result.entity.dateCreated}" type="both"
                                      timeStyle="short" dateStyle="medium"/><br/>
                </div>
            </c:forEach>
        </c:otherwise>
    </c:choose>

    <c:if test="${results.totalPages > 1}">
        <div class="container mt-2">
            <ul class="pagination justify-content-center">
                <c:set value="" var="disabledPrevious"/>
                <c:set value="" var="disabledNext"/>
                <c:if test="${results.first}">
                    <c:set value="disabled" var="disabledPrevious"/>
                </c:if>
                <c:if test="${results.last}">
                    <c:set value="disabled" var="disabledNext"/>
                </c:if>

                <li class="page-item ${disabledPrevious}">
                    <a class="page-link" href="<c:url value="/ticket/search">
                                                <c:param name="query" value="${searchForm.query}"/>
                                                <c:param name="paging.page" value="${results.number}"/>
                                                <c:param name="paging.size" value="10"/>
                                           </c:url>">&laquo;</a>
                </li>
                <c:forEach begin="1" end="${results.totalPages}" var="i">
                    <c:set value="" var="pageActive"/>
                    <c:if test="${(i - 1)   == results.number}">
                        <c:set value="active" var="pageActive"/>
                    </c:if>
                    <li class="page-item ${pageActive}">
                        <a class="page-link" href="<c:url value="/ticket/search">
                                                <c:param name="query" value="${searchForm.query}"/>
                                                <c:param name="paging.page" value="${i}"/>
                                                <c:param name="paging.size" value="10"/>
                                           </c:url>">${i}</a>
                    </li>
                </c:forEach>
                <li class="page-item ${disabledNext}">
                    <a class="page-link" href="<c:url value="/ticket/search">
                                                <c:param name="query" value="${searchForm.query}"/>
                                                <c:param name="paging.page" value="${results.number + 2}"/>
                                                <c:param name="paging.size" value="10"/>
                                           </c:url>">&raquo;</a>
                </li>
            </ul>
        </div>
    </c:if>
</template:basic>