<%--@elvariable id="tickets" type="org.springframework.data.domain.Page<com.linefight.site.entities.Ticket>"--%>
<spring:message code="title.ticketList" var="listTitle"/>
<template:basic htmlTitle="${listTitle}" bodyTitle="${listTitle}">
    <c:choose>
        <c:when test="${tickets.totalElements == 0}">
            <div class="container mt-2">
                <i><spring:message code="message.ticketList.none"/></i>
            </div>
        </c:when>
        <c:otherwise>
            <c:forEach items="${tickets.content}" var="result">
                <div class="container mt-2">
                    <spring:message code="message.ticketList.ticket"/>&nbsp;${result.id}:
                    <a href="<c:url value="/ticket/show/${result.id}"/>">
                        <c:out value="${utils:abbreviateString(result.subject, 60)}"/>
                    </a>
                    <c:out value="${result.customer.login}"/>&nbsp;<spring:message code="message.ticketList.created"/>&nbsp;
                    <utils:formatDate value="${result.dateCreated}" type="both"
                                      timeStyle="short" dateStyle="medium"/><br/>
                </div>
            </c:forEach>
        </c:otherwise>
    </c:choose>

    <c:if test="${tickets.totalPages > 1}">
        <div class="container mt-2">
            <ul class="pagination justify-content-center">
                <c:set value="" var="disabledPrevious"/>
                <c:set value="" var="disabledNext"/>
                <c:if test="${tickets.first}">
                    <c:set value="disabled" var="disabledPrevious"/>
                </c:if>
                <c:if test="${tickets.last}">
                    <c:set value="disabled" var="disabledNext"/>
                </c:if>

                <li class="page-item ${disabledPrevious}">
                    <a class="page-link" href="<c:url value="/ticket/list">
                                                <c:param name="paging.page" value="${tickets.number}"/>
                                                <c:param name="paging.size" value="10"/>
                                           </c:url>">&laquo;</a>
                </li>
                <c:forEach begin="1" end="${tickets.totalPages}" var="i">
                    <c:set value="" var="pageActive"/>
                    <c:if test="${(i - 1)   == tickets.number}">
                        <c:set value="active" var="pageActive"/>
                    </c:if>
                    <li class="page-item ${pageActive}">
                        <a class="page-link" href="<c:url value="/ticket/list">
                                                <c:param name="paging.page" value="${i}"/>
                                                <c:param name="paging.size" value="10"/>
                                           </c:url>">${i}</a>
                    </li>
                </c:forEach>
                <li class="page-item ${disabledNext}">
                    <a class="page-link" href="<c:url value="/ticket/list">
                                                <c:param name="paging.page" value="${tickets.number + 2}"/>
                                                <c:param name="paging.size" value="10"/>
                                           </c:url>">&raquo;</a>
                </li>
            </ul>
        </div>
    </c:if>
</template:basic>