<%--@elvariable id="chatSessionId" type="long"--%>
<spring:message code="title.chat" var="chatTitle"/>
<template:basic htmlTitle="${chatTitle}" bodyTitle="${chatTitle}">
    <jsp:attribute name="extraHeadContent">
        <link rel="stylesheet"
              href="<c:url value="/resources/stylesheet/chat.css" />"/>
    </jsp:attribute>
    <jsp:body>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="container pre-scrollable" id="chatLog"></div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-12 col-md-8" id="messageContainer">
                    <textarea class="form-control" id="messageArea"></textarea>
                </div>
                <div class="col align-self-end" id="buttonContainer">
                    <div class="btn-group">
                        <button class="btn btn-primary" onclick="send();"><spring:message
                                code="button.chat.send"/></button>
                        <button class="btn" onclick="disconnect();"><spring:message
                                code="button.chat.disconnect"/></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalError" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><spring:message code="window.chat.error"/></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="modalErrorBody">
                        <spring:message code="error.chat.unknown"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><spring:message
                                code="button.chat.error.dismiss"/></button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            "use strict";

            function scrollDownElement(element, pixels) {
                element.animate({scrollTop: pixels}, 200);
            }

            let send, disconnect;
            $(document).ready(function () {
                const modalError = $("#modalError");
                const modalErrorBody = $("#modalErrorBody");
                const chatLog = $("#chatLog");
                const messageArea = $("#messageArea");
                const login = "${pageContext.request.userPrincipal.name}";
                let otherJoined = false;

                if (!("WebSocket" in window)) {
                    modalErrorBody.text("<spring:message code="error.chat.websocket.unsupported" javaScriptEscape="true"/>");
                    modalError.modal("show");
                    return;
                }

                const infoMessage = function (m) {
                    chatLog.append($("<div style='display: none'>").addClass("informational")
                        .text(moment().format("h:mm:ss a") + ": " + m).fadeIn(300));
                    scrollDownElement(chatLog, 300);
                };
                infoMessage("<spring:message code="message.chat.connecting" javaScriptEscape="true"/>");

                const objectMessage = function (message) {
                    const log = $("<div style='display: none;'>");
                    const date = message.timestamp == null ? '' :
                        moment.unix(message.timestamp).format('h:mm:ss a');
                    if (message.user != null) {
                        const c = message.user === login ? "user-me" : "user-you";
                        log.append($("<span>").addClass(c)
                            .text(date + " " + message.user + ":\xA0"))
                            .append($("<span>").text(messageContent(message)));
                    } else {
                        log.addClass(message.type === "ERROR" ? "error" : "informational")
                            .text(date + " " + messageContent(message));
                    }
                    chatLog.append(log);
                    log.fadeIn(300);
                    scrollDownElement(chatLog, 300);
                };

                const messageContent = function (message) {
                    return message.userContent != null &&
                    message.userContent.length > 0 ?
                        decodeURIComponent(message.userContent) : decodeURIComponent(message.localizedContent);
                };

                let server;
                try {
                    server = new WebSocket("ws://" + window.location.host +
                        "<c:url value="/chat/${chatSessionId}"/>");
                    server.binaryType = "arraybuffer";
                } catch (error) {
                    modalErrorBody.text(error);
                    modalError.modal("show");
                    return;
                }

                server.onopen = function (event) {
                    infoMessage("<spring:message code="message.chat.connected" javaScriptEscape="true"/>");
                };

                server.onclose = function (event) {
                    if (server != null) {
                        infoMessage("<spring:message code="message.chat.disconnected" javaScriptEscape="true"/>");
                    }
                    server = null;
                    if (!event.wasClean || event.code !== 1000) {
                        modalErrorBody.text("<spring:message code="error.chat.code" javaScriptEscape="true"/> " + event.code + ": " + event.reason);
                        modalError.modal("show");
                    }
                };

                server.onerror = function (event) {
                    modalErrorBody.text(event.data);
                    modalError.modal("show");
                };

                server.onmessage = function (event) {
                    if (event.data instanceof ArrayBuffer) {
                        const message = JSON.parse(String.fromCharCode
                            .apply(null, new Uint8Array(event.data)));
                        objectMessage(message);
                        if (message.type === "JOINED") {
                            otherJoined = true;
                            if (login !== message.user) {
                                infoMessage("<spring:message code="message.chat.joined" javaScriptEscape="true"/>"
                                    .replace("{0}", message.user));
                            }
                        }
                    } else {
                        modalErrorBody.text("<spring:message code="error.chat.unexpected.type" javaScriptEscape="true"/>"
                            .replace("{0}", typeof(event.data)));
                        modalError.modal("show");
                    }
                };

                send = function () {
                    if (server == null) {
                        modalErrorBody.text("<spring:message code="error.chat.not.connected" javaScriptEscape="true"/>");
                        modalError.modal("show");
                    } else if (!otherJoined) {
                        modalErrorBody.text("<spring:message code="error.chat.not.joined" javaScriptEscape="true"/>");
                        modalError.modal("show");
                    } else if (messageArea.get(0).value.trim().length > 0) {
                        const message = {
                            timestamp: new Date(),
                            type: "TEXT",
                            user: login,
                            userContent: encodeURIComponent(messageArea.get(0).value)
                        };
                        try {
                            const json = JSON.stringify(message);
                            const length = json.length;
                            const buffer = new ArrayBuffer(length);
                            const array = new Uint8Array(buffer);
                            for (let i = 0; i < length; i++) {
                                array[i] = json.charCodeAt(i);
                            }
                            server.send(buffer);
                            messageArea.get(0).value = "";
                        } catch (error) {
                            modalErrorBody.text(error);
                            modalError.modal("show");
                        }
                    }
                };

                disconnect = function () {
                    if (server != null) {
                        infoMessage("<spring:message code="message.chat.disconnected" javaScriptEscape="true"/>");
                        server.close();
                        server = null;
                    }
                };

                window.onbeforeunload = disconnect;
            });
        </script>
    </jsp:body>
</template:basic>