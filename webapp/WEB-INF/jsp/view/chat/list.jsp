<%--suppress XmlPathReference --%>
<%--@elvariable id="sessions" type="java.util.List<com.linefight.site.chat.model.ChatSession>"--%>
<spring:message code="title.chatList" var="chatTitle"/>
<template:basic htmlTitle="${chatTitle}" bodyTitle="${chatTitle}">
    <c:choose>
        <c:when test="${fn:length(sessions) == 0}">
            <div class="container">
                <i><spring:message code="message.chatList.none"/></i>
            </div>
        </c:when>
        <c:otherwise>
            <div class="container">
                <p><spring:message code="message.chatList.instruction"/></p>
            </div>
            <security:authorize access="hasAuthority('START_CHAT')" var="canJoin"/>
            <c:forEach items="${sessions}" var="s">
                <div class="container">
                    <c:choose>
                        <c:when test="${canJoin}">
                            <a href="javascript:void 0;" onclick="join(${s.sessionId});">${s.customerLogin}</a>
                        </c:when>
                        <c:otherwise>
                            ${s.customerLogin}
                        </c:otherwise>
                    </c:choose>
                </div>
            </c:forEach>
        </c:otherwise>
    </c:choose>
    <script>
        function join(id) {
            postInvisibleForm("<c:url value="/chat/join/{id}"/>".replace("{id}", id), {});
        }
    </script>
</template:basic>