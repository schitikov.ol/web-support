<%--@elvariable id="validationErrors" type="java.util.Set<javax.validation.ConstraintViolation>"--%>

<template:main htmlTitle="Login">
    <jsp:attribute name="headContent">
        <link rel="stylesheet" href="<c:url value="/resources/stylesheet/login.css"/>"/>
    </jsp:attribute>
    <jsp:body>
        <div class="container absolute-center">
            <div class="container absolute-center">
                <h1><spring:message code="title.web.support"/></h1>
                    <%--@elvariable id="loginForm" type="com.linefight.site.controller.web.AuthenticationController.LoginForm"--%>
                <form:form method="post" modelAttribute="loginForm" autocomplete="off">
                    <div class="form-group">
                        <form:label path="login" for="login"><spring:message code="field.login.login"/>:</form:label>
                        <form:input path="login" id="login" cssClass="form-control"/>
                        <form:errors path="login" cssClass="d-block alert alert-danger mt-2"/>
                    </div>
                    <div class="form-group">
                        <form:label path="password" for="password"><spring:message
                                code="field.login.password"/>:</form:label>
                        <form:password path="password" id="password" cssClass="form-control"/>
                        <form:errors path="password" cssClass="d-block alert alert-danger mt-2"/>
                    </div>
                    <input type="submit" class="btn btn-primary" value="<spring:message code="field.login.submit"/>"/>
                </form:form>
                <c:if test="${param.containsKey('loginFailed')}">
                    <div class="mt-2 alert alert-danger">
                        <spring:message code="error.login.failed"/>
                    </div>
                </c:if>
                <c:if test="${param.containsKey('loggedOut')}">
                    <div class="mt-2 alert alert-info">
                        <spring:message code="message.login.loggedOut"/>
                    </div>
                </c:if>
                <c:if test="${validationErrors != null}">
                    <ul class="list-group mt-2">
                        <c:forEach items="${validationErrors}" var="error">
                            <li class="list-group-item list-group-item-danger">
                                <c:out value="${error.message}"/>
                            </li>
                        </c:forEach>
                    </ul>
                </c:if>
            </div>
        </div>
    </jsp:body>
</template:main>
