<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/base.jspf" %>
<%@ attribute name="htmlTitle" type="java.lang.String" rtexprvalue="true" required="true" %>
<%@ attribute name="bodyTitle" type="java.lang.String" rtexprvalue="true"
              required="true" %>
<%@ attribute name="extraHeadContent" fragment="true" required="false" %>
<%@ attribute name="extraNavbarContent" fragment="true" required="false" %>
<template:main htmlTitle="${htmlTitle}">
    <jsp:attribute name="headContent">
        <jsp:invoke fragment="extraHeadContent"/>
    </jsp:attribute>
    <jsp:body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="<c:url value="/"/>"><spring:message code="title.web.support"/></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarContent">
                <ul class="navbar-nav mr-auto">
                    <security:authorize access="hasAuthority('VIEW_TICKETS')">
                        <li class="nav-item">
                            <a class="nav-link" href="<c:url value="/ticket/list"/>"><spring:message
                                    code="nav.item.list.tickets"/></a>
                        </li>
                    </security:authorize>
                    <security:authorize access="hasAuthority('CREATE_TICKET')">
                        <li class="nav-item">
                            <a class="nav-link" href="<c:url value="/ticket/create"/>"><spring:message
                                    code="nav.item.create.ticket"/></a>
                        </li>
                    </security:authorize>
                    <security:authorize access="hasAuthority('CREATE_CHAT_REQUEST')">
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:void 0;" onclick="newChat()"><spring:message
                                    code="nav.item.chat.support"/></a>
                        </li>
                    </security:authorize>
                    <security:authorize access="hasAuthority('VIEW_CHAT_REQUESTS')">
                        <li class="nav-item">
                            <a class="nav-link" href="<c:url value="/chat/list"/>"><spring:message
                                    code="nav.item.view.chat"/></a>
                        </li>
                    </security:authorize>
                    <security:authorize access="hasAuthority('VIEW_USER_SESSIONS')">
                        <li class="nav-item">
                            <a class="nav-link" href="<c:url value="/session/list"/>"><spring:message
                                    code="nav.item.list.session"/></a>
                        </li>
                    </security:authorize>
                    <jsp:invoke fragment="extraNavbarContent"/>
                </ul>
                <ul class="navbar-nav">
                    <security:authorize access="hasAuthority('VIEW_TICKETS')">
                        <%--@elvariable id="searchForm" type="com.linefight.site.controller.web.main.TicketController.SearchForm"--%>
                        <c:url value="/ticket/search" var="searchFormAction"/>
                        <form:form cssClass="form-inline" modelAttribute="searchForm" method="get"
                                   action="${searchFormAction}"
                                   autocomplete="off">
                            <spring:message code="nav.item.search.placeholder" var="placeholder"/>
                            <form:input path="query" cssClass="form-control mr-sm-2" type="search"
                                        placeholder="${placeholder}" id="search"/>
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><spring:message
                                    code="nav.item.search"/></button>
                        </form:form>
                    </security:authorize>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                            <i class="fas fa-globe"></i>&nbsp;<spring:message code="nav.item.language"/>
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void 0;"
                               onclick="postInvisibleForm('<c:url value="/"/>', {locale: 'en_US'})">English</a>
                            <a class="dropdown-item" href="javascript:void 0;"
                               onclick="postInvisibleForm('<c:url value="/"/>', {locale: 'ru'})">Русский</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link">
                            <i class="far fa-user"></i>
                            <security:authentication property="principal.login"/>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-primary d-inline-block"
                           href="javascript:void 0;"
                           onclick="postInvisibleForm('<c:url value="/logout"/>', {})"><spring:message
                                code="nav.item.logout"/></a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="container mt-2">
            <h1><c:out value="${bodyTitle}"/></h1>
        </div>
        <jsp:doBody/>
    </jsp:body>
</template:main>
