<%--@elvariable id="_csrf" type="org.springframework.security.web.csrf.CsrfToken"--%>
<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/base.jspf" %>
<%@ attribute name="htmlTitle" type="java.lang.String" required="true" rtexprvalue="true" %>
<%@ attribute name="headContent" fragment="true" required="false" %>
<!DOCTYPE html>
<html>
<head>
    <title><c:out value="${htmlTitle}"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" lang="javascript" src="<c:url value="/resources/script/jquery-3.3.1.js"/>"></script>
    <script type="text/javascript" lang="javascript" src="<c:url value="/resources/script/jquery-ui.js"/>"></script>
    <script type="text/javascript" lang="javascript" src="<c:url value="/resources/script/bootstrap.js"/>"></script>
    <script type="text/javascript" lang="javascript" src="<c:url value="/resources/script/moment.js"/>"></script>
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/stylesheet/bootstrap.css" />"/>
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/stylesheet/fontawesome-all.css"/>"/>
    <link type="text/css" rel="stylesheet" href="<c:url value="/resources/stylesheet/jquery-ui.css"/>"/>
    <script type="text/javascript" lang="javascript">
        function postInvisibleForm(url, fields) {
            const form = $("<form id='mapForm' method='post'></form>")
                .attr({action: url, style: "display: none;"});
            for (let key in fields) {
                if (fields.hasOwnProperty(key)) {
                    form.append($("<input type='hidden'>").attr({
                        name: key, value: fields[key]
                    }));
                }
            }
            form.append($("<input type='hidden'>").attr({
                name: "${_csrf.parameterName}", value: "${_csrf.token}"
            }));
            $("body").append(form);
            form.submit();
        }

        function newChat() {
            postInvisibleForm("<c:url value="/chat/new"/>");
        }

        $(function () {
            $("#search").keyup(function () {
                $.ajax("<c:url value="/services/rest/ticket/search"/>", {
                    dataType: "json",
                    data: {
                        query: $("#search").val()
                    },
                }).done(function (response) {
                    let results = new Set();
                    for (let r of response.results) {
                        results.add(r.entity.subject);
                        results.add(r.entity.customer.login);
                    }

                    $("#search").autocomplete({source: [...results]});
                });
            });
        })
    </script>
    <jsp:invoke fragment="headContent"/>
</head>
<body>
<jsp:doBody/>


</body>
</html>